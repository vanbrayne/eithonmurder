/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.misc;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class BossBar {

	public static void sendObjective(final MurderPlayer mp) {
		if (!Bukkit.getPluginManager().isPluginEnabled("BossBarAPI")) { return; }
		String message = "????";
		if (mp.isMurderer()) {
			message = Messages.OBJECTIVE_MURDERER.forPlayer(mp.player());
		}
		if (mp.isBystander()) {
			message = Messages.OBJECTIVE_BYSTANDER.forPlayer(mp.player());
		}
		if (mp.isWeaponBystander()) {
			message = Messages.OBJECTIVE_BYSTANDER_WEAPON.forPlayer(mp.player());
		}

		final String msg = message;

		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				org.inventivetalent.bossbar.BossBarAPI.setMessage(mp.player(), msg);
			}
		}, 5);

		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				org.inventivetalent.bossbar.BossBarAPI.removeBar(mp.player());
			}
		}, 180);
	}

}
