/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.misc;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Titles {

	private static int IN = 20;
	private static int STAY = (Murder.countdown - 2) * 20;
	private static int OUT = 20;

	public static void sendRoleTitle(MurderPlayer mp) {
		if (!Bukkit.getPluginManager().isPluginEnabled("TitleManager")) { return; }
		try {
			String[] message = new String[] {
					"{\"text\":\"\",\"extra\":[{\"text\":\"\"}]}",
					"{\"text\":\"\",\"extra\":[{\"text\":\"\"}]}" };
			if (mp.isMurderer()) {
				message = new String[] {
						Messages.TITLE_MURDERER.forPlayer(mp.player()),
						null };
			} else if (mp.isWeaponBystander()) {
				message = new String[] {
						Messages.TITLE_BYSTANDER.forPlayer(mp.player()),
						Messages.TITLE_BYSTANDER_WEAPON.forPlayer(mp.player()) };
			} else if (mp.isBystander()) {
				message = new String[] {
						Messages.TITLE_BYSTANDER.forPlayer(mp.player()),
						null };
			}

			de.inventivegames.util.title.TitleManager.reset(mp.player());

			String title = message[0];
			String subtitle = message[1];

			de.inventivegames.util.title.TitleManager.sendTimings(mp.player(), IN, STAY, OUT);

			if (subtitle != null) {
				de.inventivegames.util.title.TitleManager.sendSubTitle(mp.player(), subtitle);
			}
			de.inventivegames.util.title.TitleManager.sendTitle(mp.player(), title);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendIdentityTitle(MurderPlayer mp) {
		if (!Bukkit.getPluginManager().isPluginEnabled("TitleManager")) { return; }
		try {
			String[] message = new String[] {
					"{\"text\":\"\",\"extra\":[{\"text\":\"\"}]}",
					"{\"text\":\"\",\"extra\":[{\"text\":\"\"}]}" };
			message[1] = String.format(Messages.TITLE_IDENTITY.forPlayer(mp.player()), mp.getNameTag());

			de.inventivegames.util.title.TitleManager.reset(mp.player());

			String subtitle = message[1];

			de.inventivegames.util.title.TitleManager.sendTimings(mp.player(), 20, 40, 20);

			de.inventivegames.util.title.TitleManager.sendSubTitle(mp.player(), subtitle);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
