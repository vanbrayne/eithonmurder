/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder;

import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.bungeecord.BungeeCordHandler;
import de.inventivegames.murder.commands.CommandHandler;
import de.inventivegames.murder.game.ChatManager;
import de.inventivegames.murder.game.Game;
import de.inventivegames.murder.game.Spectate;
import de.inventivegames.murder.game.corpse.Corpses;
import de.inventivegames.murder.listeners.PacketListener;
import de.inventivegames.murder.listeners.PlayerListener;
import de.inventivegames.murder.listeners.ResourcePackListener;
import de.inventivegames.murder.listeners.WorldListener;
import de.inventivegames.murder.loggers.KillLogger;
import de.inventivegames.murder.misc.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.inventivetalent.bossbar.MetricsLite;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Murder extends JavaPlugin implements Listener {

	private static String permBase    = "murder.";
	private static String prefix      = "§1[§4Murder§1] ";
	private static String debugPrefix = "§1[§4Murder§7|§aDEBUG§1]§r ";
	private static ConsoleCommandSender console;
	private static Murder               instance;

	public static String TEAM_PREFIX = "m_d_";

	static        File   configFile = new File("plugins/Murder/config.yml");
	public static Random rd         = new Random();
	public static String serverVersion;
	private static String[] nameTags       = {
			"Alfa ",
			"Bravo ",
			"Charlie ",
			"Delta ",
			"Echo ",
			"Foxtrot ",
			"Golf ",
			"Hotel ",
			"India ",
			"Juliett ",
			"Kilo ",
			"Lima ",
			"Miko ",
			"November ",
			"Oscar ",
			"Papa ",
			"Quebec ",
			"Romeo ",
			"Sierra ",
			"Tango ",
			"Uniform ",
			"Victor ",
			"Whiskey ",
			"X-ray ",
			"Yankee ",
			"Zulu " };
	private static String[] colorCode      = {
			"§1 ",
			"§2 ",
			"§3 ",
			"§4 ",
			"§5 ",
			"§6 ",
			"§7 ",
			"§8 ",
			"§9 ",
			"§a ",
			"§b ",
			"§c ",
			"§d ",
			"§e " };
	public static  int      minPlayers     = -1;
	public static  int      maxPlayers     = -1;
	public static  int      smokeTimer     = -1;
	public static  int      lobbyCountdown = -1;
	public static  int      countdown      = -1;

	// BungeeCordSettings
	public static boolean bungeeCord  = false;
	public static String  bungeeGame  = "murder";
	public static String  bungeeHub   = "lobby";
	public static int     bungeeArena = 1;

	public static int POINTS_PLUS                 = 2;                                                                                                                                                                                                                                                                                // MurdererKill
	public static int POINTS_MINUS                = 20;                                                                                                                                                                                                                                                                                // KilledInnocent
	public static int POINTS_MURDERER_WIN         = 40;                                                                                                                                                                                                                                                                                // MurdererWon
	public static int POINTS_BYSTANDER_WIN_WEAPON = 40;                                                                                                                                                                                                                                                                                // KilledMurderer
	public static int POINTS_BYSTANDER_WIN        = 2;                                                                                                                                                                                                                                                                                // BystandersWon

	public static int min_loot_delay = 15;
	public static int max_loot_delay = 55;

	public static CommandHandler    cmdHandler;
	public static BungeeCordHandler bungeeHandler;

	public static ArrayList<Game> games = new ArrayList<Game>();

	public static ArrayList<Player> murdererBlacklist = new ArrayList<Player>();
	public static ArrayList<Player> forcedMurderers   = new ArrayList<Player>();

	public static ArrayList<Player> weaponBlacklist = new ArrayList<Player>();
	public static ArrayList<Player> forcedWeapons   = new ArrayList<Player>();

	static String[] depend     = new String[] {
			"ParticleLIB:2067",
			"NickNamer:5341",
			"PacketListenerApi:2930" };
	static String[] softdepend = new String[] {
			"ResourcePackApi:2397",
			"TitleManager:1047",
			"NPCLib:5853",
			"BossBarAPI:7504" };

	@Override
	public void onEnable() {
		instance = this;
		console = instance.getServer().getConsoleSender();

		// Check dependencies
		Map<String, String> missingDepend = new HashMap<>();
		for (String s : depend) {
			String[] split = s.split(":");
			if (!Bukkit.getPluginManager().isPluginEnabled(split[0])) {
				missingDepend.put(split[0], split[1]);
			}
		}
		if (!missingDepend.isEmpty()) {
			console.sendMessage(prefix + "§c****************************************");
			console.sendMessage(prefix);
			console.sendMessage(prefix + "§cYou are missing the following dependencies:");
			for (Entry<String, String> entry : missingDepend.entrySet()) {
				console.sendMessage(prefix + "§7 - " + entry.getKey());
				console.sendMessage(prefix + "§8Download: http://www.spigotmc.org/resources/" + entry.getValue());
			}
			console.sendMessage(prefix);
			console.sendMessage(prefix + "§c****************************************");

			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		for (String s : softdepend) {
			String[] split = s.split(":");
			if (!Bukkit.getPluginManager().isPluginEnabled(split[0])) {
				console.sendMessage(prefix + "§cIt is recommended to install §7" + split[0] + " (§8 http://www.spigotmc.org/resources/" + split[1] + " §7)");
			}
		}

		if (!Reflection.getVersion().contains("1_7") && !Reflection.getVersion().contains("1_8")) {
			System.err.println("[Murder] This Server is not running a compatible Bukkit/Spigot version! Please update your Server or contact the Author of this Plugin.");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		cmdHandler = new CommandHandler();
		registerEvents(instance, instance, new PlayerListener(), new WorldListener(), new PacketListener(), new Signs(), new Spectate(), new ChatManager(), new KillLogger(), cmdHandler);

		if (Bukkit.getPluginManager().isPluginEnabled("ResourcePackApi")) {
			Bukkit.getPluginManager().registerEvents(new ResourcePackListener(), this);
		}
		if (Bukkit.getPluginManager().isPluginEnabled("NPCLib")) {
			Bukkit.getPluginManager().registerEvents(new Corpses(), this);
		}

		instance.getCommand("murder").setExecutor(cmdHandler);

		try {
			de.inventivegames.util.message.Messages.init(new File(this.getDataFolder(), "messages.yml"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.saveDefaultConfig();

		initMetrics();

		minPlayers = instance.getConfig().getInt("MinPlayers");
		maxPlayers = instance.getConfig().getInt("MaxPlayers");
		smokeTimer = instance.getConfig().getInt("SmokeDelay");

		min_loot_delay = instance.getConfig().getInt("loot.delay.min");
		max_loot_delay = instance.getConfig().getInt("loot.delay.max");

		lobbyCountdown = instance.getConfig().getInt("lobbyCountdown");
		countdown = instance.getConfig().getInt("countdown");

		if (countdown < 5) {
			console.sendMessage(prefix + "§cThe low lobby countdown (" + countdown + ") may cause problems!");
		}

		bungeeCord = instance.getConfig().getBoolean("useBungeeCord");
		bungeeGame = instance.getConfig().getString("BungeeCordGameServerName");
		bungeeHub = instance.getConfig().getString("BungeeCordHubName");
		bungeeArena = instance.getConfig().getInt("BungeeCordArena");

		serverVersion = instance.getServer().getBukkitVersion().toString();
		if (bungeeCord) {
			Bukkit.getServer().getPluginManager().registerEvents(bungeeHandler = new BungeeCordHandler(), instance);
			Bukkit.getMessenger().registerOutgoingPluginChannel(instance, "BungeeCord");
		}

		ArenaManager.loadArenas();

		try {
			SkinStorage.load(colorCode);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

		console.sendMessage(prefix + "§aEnabled §cMurder§r");
		console.sendMessage(prefix + "§7" + this.getDescription().getDescription() + "§r");
		//
		// try {
		// AccessUtil.setAccessible(NPCLibPlugin.class.getDeclaredField("useNickNamer")).set(null, false);
		// } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
		// e.printStackTrace();
		// }
	}

	private static void initMetrics() {
		try {
			final MetricsLite metrics = new MetricsLite(instance);
			metrics.start();
		} catch (final IOException e) {
			// Failed to submit the stats :-(
		}
	}

	@Override
	public void onDisable() {
		Bukkit.getScheduler().cancelTasks(Murder.getInstance());
		ArenaManager.saveArenas();
	}

	public static void reload() {
		ArenaManager.saveArenas();

		ArenaManager.unloadArenas();

		getInstance().reloadConfig();
		try {
			de.inventivegames.util.message.Messages.init(new File(instance.getDataFolder(), "messages.yml"));
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}

		minPlayers = instance.getConfig().getInt("MinPlayers");
		maxPlayers = instance.getConfig().getInt("MaxPlayers");
		smokeTimer = instance.getConfig().getInt("SmokeDelay");

		lobbyCountdown = instance.getConfig().getInt("lobbyCountdown");
		countdown = instance.getConfig().getInt("countdown");

		Bukkit.getScheduler().cancelTasks(Murder.getInstance());

		ArenaManager.loadArenas();
	}

	public static void addName(String name) {
		if (name.length() + 4 > 16) { return; }

		final int length = nameTags.length;
		final String[] New = new String[length + 1];
		for (int i = 0; i < length; i++) {
			New[i] = nameTags[i];
		}
		nameTags = null;
		New[length] = name;
		nameTags = new String[length + 1];
		for (int i = 0; i < length + 1; i++) {
			nameTags[i] = New[i];
		}
	}

	public static void registerEvents(Plugin plugin, Listener... listeners) {
		for (final Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}

	public static String getPermBase() {
		return permBase;
	}

	public static String getPrefix() {
		return prefix;
	}

	public static String[] getColorCode() {
		return colorCode;
	}

	public static String getDebugPrefix() {
		return debugPrefix;
	}

	public static ConsoleCommandSender getConsole() {
		return console;
	}

	public static Murder getInstance() {
		return instance;
	}

	public static File getConfigFile() {
		return configFile;
	}

	public static Random getRd() {
		return rd;
	}

	public static String getServerVersion() {
		return serverVersion;
	}

	public static String[] getNameTags() {
		return nameTags;
	}

	public static int getMinPlayers() {
		return minPlayers;
	}

	public static int getMaxPlayers() {
		return maxPlayers;
	}

	public static int getSmokeTimer() {
		return smokeTimer;
	}

	public static int getLobbyCountdown() {
		return lobbyCountdown;
	}

	public static int getCountdown() {
		return countdown;
	}

	public static boolean isBungeeCord() {
		return bungeeCord;
	}

	public static String getBungeeHub() {
		return bungeeHub;
	}

	public static int getBungeeArena() {
		return bungeeArena;
	}

	public static int getPOINTS_PLUS() {
		return POINTS_PLUS;
	}

	public static int getPOINTS_MINUS() {
		return POINTS_MINUS;
	}

	public static int getPOINTS_MURDERER_WIN() {
		return POINTS_MURDERER_WIN;
	}

	public static int getPOINTS_BYSTANDER_WIN_WEAPON() {
		return POINTS_BYSTANDER_WIN_WEAPON;
	}

	public static int getPOINTS_BYSTANDER_WIN() {
		return POINTS_BYSTANDER_WIN;
	}

	public static CommandHandler getCmdHandler() {
		return cmdHandler;
	}

}
