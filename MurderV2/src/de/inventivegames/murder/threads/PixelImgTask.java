/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.threads;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.game.Spectate;
import de.inventivegames.utils.pixelimage.ImageChar;
import de.inventivegames.utils.pixelimage.PixelImage;
import org.bukkit.entity.Player;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class PixelImgTask extends Thread {

	private static HashMap<Player, PixelImgTask> threads = new HashMap<>();

	protected static BufferedImage STEVE;

	public static void cancelThread(Player p) {
		if (threads.containsKey(p)) {
			threads.get(p).interrupt();
			threads.remove(p);
		}
	}

	public PixelImgTask(Player p) {
		this.p = p;
		threads.put(this.p, this);
	}

	private final Player p;

	@Override
	public void run() {
		Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Trying to Download Skin for " + this.p.getName());
		String[] lines = new String[8];
		if (Spectate.faces.containsKey(this.p)) {
			lines = Spectate.faces.get(this.p);
		} else {

			BufferedImage img = null;
			try {
				final URL url = new URL("https://minotar.net/avatar/" + this.p.getName() + "/32");
				final URLConnection conn = url.openConnection();
				conn.setConnectTimeout(500);
				final InputStream is = conn.getInputStream();
				img = ImageIO.read(is);
				is.close();
			} catch (final Exception e) {
				Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Could not download Skin for " + this.p.getName());
				if (STEVE == null) {
					try {
						final URL url = new URL("https://minotar.net/avatar/steve/32");
						final URLConnection conn = url.openConnection();
						conn.setConnectTimeout(500);
						final InputStream is = conn.getInputStream();
						STEVE = ImageIO.read(is);
						is.close();
					} catch (final Exception e1) {
						Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Could not download Steve Skin");
					}
				}
				img = STEVE;
			}
			try {
				final PixelImage pimg = new PixelImage(img, 8, ImageChar.BLOCK.getChar());

				Spectate.faces.put(this.p, pimg.getLines());

				lines = pimg.getLines();
			} catch (final Exception ex) {
			}
		}
		Spectate.faces.put(this.p, lines);
		threads.remove(this.p);
	}

	public static void cancelThreads() {
		for (Player p : threads.keySet()) {
			threads.get(p).interrupt();
			;
		}
		threads.clear();
	}

}
