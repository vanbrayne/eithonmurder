/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.game;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.SkinStorage;
import de.inventivegames.murder.arena.Arena;
import de.inventivegames.murder.event.GameJoinEvent;
import de.inventivegames.murder.event.LanguageRequestEvent;
import de.inventivegames.murder.misc.Reflection;
import de.inventivegames.murder.misc.ResourcePack;
import de.inventivegames.nickname.AccessUtil;
import de.inventivegames.nickname.Nicks;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class MurderPlayer {

	private final Player p;
	private       Arena  arena;
	public        String nameTag;
	public        String disguisedTag;

	private boolean bystander = false;
	private boolean murderer  = false;
	private boolean weapon    = false;

	private boolean inGame     = false;
	private boolean inLobby    = false;
	private boolean inSpectate = false;

	private boolean canSprint = false;

	private boolean disguised = false;

	private static HashMap<Player, MurderPlayer> murderPlayers = new HashMap<Player, MurderPlayer>();

	private static Class<?> nmsChatSerializer    = Reflection.getNMSClass(Reflection.getVersion().contains("1_8_R2") || Reflection.getVersion().contains("1_8_R3") ? "IChatBaseComponent$ChatSerializer" : "ChatSerializer");
	private static Class<?> nmsPacketPlayOutChat = Reflection.getNMSClass("PacketPlayOutChat");

	private boolean cantPickup = false;

	private ItemStack[] InventoryContent      = null;
	private ItemStack[] InventoryArmorContent = null;
	private Location    prevLocation          = null;
	private GameMode    prevGamemode          = null;
	private Float       prevExp               = -1F;
	private int         prevLevel             = -1;
	private double      prevHealth            = -1;
	private double      prevFood              = -1;
	private int         points                = 0;

	public boolean resetRP = true;

	public long lastPlayed = -1;

	public boolean disguiseTimeout = false;

	public boolean downloadingRP = false;
	public boolean downloadedRP  = false;

	// ////////////////////////
	public MurderPlayer(Player p) {
		this.p = p;
		if (p == null) { return; }

		MurderPlayer.murderPlayers.put(p, this);
	}

	public static MurderPlayer getPlayer(Player p) {
		if (murderPlayers.containsKey(p)) { return murderPlayers.get(p); } else { return new MurderPlayer(p); }
	}

	public static void resetPlayers() {
		murderPlayers.clear();
	}

	// /////////////////

	public void setNameTag(String tag) {
		this.nameTag = tag;
		if (tag == null || tag.isEmpty()) {
			Nicks.removeNick(this.player().getUniqueId());
			Nicks.removeSkin(this.player().getUniqueId());
		} else {
			try {
				Nicks.setNick(this.player().getUniqueId(), tag);
				/* Inject the skin UUID into NickNamer */// TODO:Update NickNamer to be able to do this without reflection
				((Map<UUID, UUID>) AccessUtil.setAccessible(Nicks.class.getDeclaredField("skins")).get(null)).put(this.player().getUniqueId(), SkinStorage.get(this.getNameTag().substring(0, 2)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Nicks.updatePlayer(this.player());
	}

	public void disguiseMurderer(String name) {
		this.disguised = true;
		this.disguisedTag = name;

		if (name != null && !name.isEmpty()) {
			try {
				Nicks.setNick(this.player().getUniqueId(), this.disguisedTag);
				/* Inject the skin UUID into NickNamer */// TODO:Update NickNamer to be able to do this without reflection
				((Map<UUID, UUID>) AccessUtil.setAccessible(Nicks.class.getDeclaredField("skins")).get(null)).put(this.player().getUniqueId(), SkinStorage.get(this.getDisguisedTag().substring(0, 2)));
			} catch (Exception e) {
				e.printStackTrace();
			}
			Nicks.updatePlayer(this.player());
		}

		this.p.sendMessage(String.format(Messages.DISGUISED.forPlayer(this.p), "§r" + name));
	}

	public String getDisguisedTag() {
		return this.disguisedTag;
	}

	public String getNameTag() {
		return this.nameTag != null ? this.nameTag : "";
	}

	public int getPoints() {
		return this.points;
	}

	public void addPoints(int i) {
		this.setPoints(this.getPoints() + i);
	}

	public void subtractPoints(int i) {
		this.setPoints(this.getPoints() - i);
	}

	public void setPoints(int i) {
		this.points = i;
	}

	public void joinArena(Arena arena) {
		final GameJoinEvent event = new GameJoinEvent(arena, this.p);
		Murder.getInstance().getServer().getPluginManager().callEvent(event);
		this.arena = arena;
		if (event.isCancelled()) { return; }

		if (this.resetRP) {
			ResourcePack.setResourcePack(this.p);
		}

		this.InventoryContent = this.p.getInventory().getContents().clone();
		this.p.getInventory().clear();
		this.InventoryArmorContent = this.p.getInventory().getArmorContents().clone();
		this.p.getInventory().setArmorContents(null);
		this.prevLocation = this.p.getLocation().clone();
		this.prevGamemode = this.p.getGameMode();
		this.p.setGameMode(GameMode.ADVENTURE);
		this.prevLevel = this.p.getLevel();
		this.p.setLevel(0);
		this.prevExp = this.p.getExp();
		this.p.setExp(0F);
		this.prevHealth = ((Damageable) this.p).getHealth();
		((Damageable) this.p).setHealth(20D);
		this.prevFood = this.p.getFoodLevel();
		this.p.setWalkSpeed(0.2F);
		this.p.setFoodLevel(20);
		this.p.updateInventory();
		this.removeEffects();
		this.setInLobby();
		this.p.getInventory().setHeldItemSlot(0);

		this.p.setAllowFlight(false);
		this.p.setFlying(false);

		Game.join(arena.getID(), this.p);

		this.lastPlayed = System.currentTimeMillis();
	}

	public void leaveArena(boolean v) {
		this.leaveArena(true, v);
	}

	public void leaveArena(boolean broadcast, boolean v) {
		this.leaveArena(this.arena, broadcast, v);
	}

	public void leaveArena(Arena arena, boolean broadcast, boolean v) {
		if (this.playing()) {
			if (this.resetRP) {
				ResourcePack.resetResourcePack(this.p);
			}

			this.p.getInventory().clear();
			this.p.updateInventory();
			if (this.p == null) { return; }
			if (this.p.getInventory() != null) {
				if (this.InventoryContent != null) {
					this.p.getInventory().setContents(this.InventoryContent);
				}
				this.InventoryContent = null;
				if (this.InventoryArmorContent != null) {
					this.p.getInventory().setArmorContents(this.InventoryArmorContent);
				}
				this.InventoryArmorContent = null;
			}
			if (this.p.getPassenger() != null) {
				this.p.getPassenger().eject();
			}
			this.p.setPassenger(null);
			if (this.prevLocation != null) {
				this.p.teleport(this.prevLocation);
			}
			this.prevLocation = null;
			if (this.prevGamemode != null) {
				this.p.setGameMode(this.prevGamemode);
				if (this.prevGamemode != GameMode.CREATIVE) {
					this.p.setAllowFlight(false);
					this.p.setFlying(false);
				}
			}
			this.prevGamemode = null;
			this.p.setLevel(this.prevLevel >= 0 ? this.prevLevel : 0);
			this.prevLevel = -1;
			this.p.setExp(this.prevExp >= 0 ? this.prevExp : 0);
			this.prevExp = -1F;
			if (this.prevHealth <= 20) {
				if (this.prevHealth > 0) {
					this.p.setHealth(this.prevHealth);
				} else if (this.prevHealth != -1.0D) {
					this.p.setHealth(0);
				}
			} else {
				this.p.setHealth(20);
			}
			this.prevHealth = -1;
			if (this.prevFood <= 20) {
				if (this.prevFood >= 0) {
					this.p.setFoodLevel((int) this.prevFood);
				} else if (this.prevFood != -1) {
					this.p.setFoodLevel(0);
				}
			} else {
				this.p.setFoodLevel(20);
			}
			this.prevFood = -1;
			this.removeEffects();

			this.p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 255, false));

			for (final Player online : Murder.getInstance().getServer().getOnlinePlayers()) {
				online.showPlayer(this.p);
			}

			this.setLeft();

			if (arena != null) {
				Game.leave(arena.getID(), this.p, broadcast, v);
			}

		}

		this.arena = null;

		this.lastPlayed = System.currentTimeMillis();
	}

	public void sendRawMessage(String message) {
		try {
			final Object handle = Reflection.getHandle(this.p);
			final Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			final Object serialized = Reflection.getMethod(nmsChatSerializer, "a", String.class).invoke(null, message);
			final Object packet = nmsPacketPlayOutChat.getConstructor(Reflection.getNMSClass("IChatBaseComponent")).newInstance(serialized);
			Reflection.getMethod(connection.getClass(), "sendPacket").invoke(connection, packet);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void removeEffects() {
		final Player pl = this.p;
		final List<PotionEffect> effects = new ArrayList<PotionEffect>();
		for (final PotionEffect effect : pl.getActivePotionEffects()) {
			pl.removePotionEffect(effect.getType());
			effects.add(effect);
		}

		for (final PotionEffect effect : effects) {
			pl.addPotionEffect(effect);
		}
		effects.clear();
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {

						for (final PotionEffect effect : pl.getActivePotionEffects()) {
							pl.removePotionEffect(effect.getType());
						}

					}
				}, 2);
				Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {
						for (final PotionEffect effect : effects) {
							pl.addPotionEffect(new PotionEffect(effect.getType(), 2, 2));
						}
						effects.clear();
					}
				}, 20);
			}
		}, 2);

		for (final Player online : Murder.getInstance().getServer().getOnlinePlayers()) {
			online.showPlayer(pl);
		}
		pl.getInventory().setHelmet(new ItemStack(Material.AIR));
		this.p.getInventory().setHeldItemSlot(0);

	}

	public Arena getArena() {
		return this.arena;
	}

	public boolean hasRole() {
		return this.weapon || this.bystander || this.murderer;
	}

	public boolean playing() {
		if (this.inGame || this.inLobby || this.inSpectate) { return true; }
		return false;
	}

	public boolean murdererDisguised() {
		return this.disguised;
	}

	public boolean canSprint() {
		return this.canSprint;
	}

	public void setCanSprint(boolean b) {
		this.canSprint = b;
	}

	public void setInLobby() {
		this.inLobby = true;
		this.inGame = false;
		this.inSpectate = false;

		this.p.setFlying(false);
		this.p.setAllowFlight(false);
	}

	public void setInGame() {
		this.inGame = true;
		this.inLobby = false;
		this.inSpectate = false;

		this.p.setFlying(false);
		this.p.setAllowFlight(false);

	}

	public void setInSpectate() {
		this.inSpectate = true;
		this.inGame = false;
		this.inLobby = false;

		this.p.setAllowFlight(true);
		this.p.setFlying(true);
	}

	public void setLeft() {
		this.inSpectate = false;
		this.inLobby = false;
		this.inGame = false;

		this.murderer = false;
		this.weapon = false;
		this.bystander = false;

	}

	public boolean isBystander() {
		return this.bystander || this.weapon;
	}

	public void setBystander(boolean b) {
		this.bystander = b;
		this.p.setFoodLevel(6);
	}

	public boolean isWeaponBystander() {
		return this.weapon;
	}

	public void setWeaponBystander(boolean b) {
		this.weapon = b;
		this.p.setFoodLevel(6);
	}

	public boolean isMurderer() {
		return this.murderer;
	}

	public void setMurderer(boolean b) {
		this.murderer = b;
		this.p.setFoodLevel(20);
	}

	public String getRole() {
		return this.isWeaponBystander() ? "§9WeaponBystander" : this.isMurderer() ? "§cMurderer" : this.isBystander() ? "§9Bystander" : "§8NONE";
	}

	public String getStatus() {
		return this.inGame() ? "inGame" : this.inLobby() ? "inLobby" : this.inSpectate() ? "inSpectate" : "NONE";
	}

	public boolean cantPickup() {
		return this.cantPickup;
	}

	public void cantPickup(boolean b) {
		this.cantPickup = b;
	}

	public boolean inGame() {
		return this.inGame;
	}

	public boolean inLobby() {
		return this.inLobby;
	}

	public boolean inSpectate() {
		return this.inSpectate;
	}

	public String getLocale() {
		Object locale = null;
		try {
			Object handle = Reflection.getHandle(this.p);
			locale = Reflection.getField(handle.getClass(), "locale").get(handle);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		LanguageRequestEvent event = new LanguageRequestEvent(this.p, (String) locale);
		Bukkit.getPluginManager().callEvent(event);
		return event.getLanguage().substring(0, 2);
	}

	public Player player() {
		return this.p;
	}

	// ////////

	@Override
	public String toString() {
		return "MurderPlayer[player=" + this.player() + "]";
	}

	public void sendInfo(Player sender) {
		sender.sendMessage("§7---------------------------------");
		sender.sendMessage("§7>> §2Player §a" + this.player().getName() + "§r(" + this.getNameTag() + (this.isMurderer() && this.disguisedTag != null ? "§r(Disguised as " + this.disguisedTag + "§r)" : "") + "§r) §7<<");
		sender.sendMessage("§7---------------------------------");
		MurderPlayer.getPlayer(sender).sendRawMessage("{\"text\":\"\",\"extra\":[{\"text\":\"Arena=\",\"color\":\"gray\"},{\"text\":\"" + this.getArena().getID() + "/" + this.getArena().getName() + "\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/murder arenainfo " + this.getArena()
				.getID() + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"§7Click here to show Information for this Arena.\"}}]}");
		sender.sendMessage("§7Playing=§a" + this.playing());
		sender.sendMessage("§7InLobby=§a" + this.inLobby());
		sender.sendMessage("§7InGame=§a" + this.inGame());
		sender.sendMessage("§7Spectating=§a" + this.inSpectate());
		sender.sendMessage("§7Role=§a" + (this.isWeaponBystander() ? "§9WeaponBystander" : this.isMurderer() ? "§cMurderer" : this.isBystander() ? "§9Bystander" : "§8NONE"));
		sender.sendMessage("§7---------------------------------");
	}

	public static void reset(MurderPlayer mp) {
		murderPlayers.remove(mp.player());
	}

}
