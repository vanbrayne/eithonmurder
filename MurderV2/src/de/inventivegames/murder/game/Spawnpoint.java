/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.game;

import de.inventivegames.murder.Murder;
import org.bukkit.Location;
import org.bukkit.World;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Spawnpoint {

	private final Location loc;

	public Spawnpoint(Location loc) {
		this.loc = loc;
	}

	public Spawnpoint(World world, double X, double Y, double Z) {
		this.loc = new Location(world, X, Y, Z);
	}

	public World getWorld() {
		return this.loc.getWorld();
	}

	public double getX() {
		return this.loc.getX();
	}

	public double getY() {
		return this.loc.getY();
	}

	public double getZ() {
		return this.loc.getZ();
	}

	public Location toLocation() {
		return this.loc;
	}

	// ////////////

	@Override
	@SuppressWarnings("unchecked")
	public String toString() {
		final JSONObject object = new JSONObject();
		object.put("world", this.loc.getWorld().getName());
		object.put("x", this.loc.getX());
		object.put("y", this.loc.getY());
		object.put("z", this.loc.getZ());
		return object.toString();
	}

	public static Spawnpoint fromString(String s) {
		JSONObject property = new JSONObject();
		try {
			property = (JSONObject) new JSONParser().parse(s);
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		World world = null;
		double X = 0;
		double Y = 0;
		double Z = 0;

		final String wn = (String) property.get("world");
		world = Murder.getInstance().getServer().getWorld(wn);
		X = (double) property.get("x");
		Y = (double) property.get("y");
		Z = (double) property.get("z");

		return new Spawnpoint(world, X, Y, Z);
	}

}
