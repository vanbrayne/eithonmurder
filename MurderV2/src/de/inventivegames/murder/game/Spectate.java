/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.game;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.arena.Arena;
import de.inventivegames.murder.commands.Permissions;
import de.inventivegames.murder.threads.PixelImgTask;
import de.inventivegames.util.message.Messages;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Spectate implements Listener {
	private final String                    INV_TITLE = "§7Teleporter";
	public static HashMap<Player, String[]> faces     = new HashMap<Player, String[]>();

	@EventHandler
	public void onCompassUse(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing() && e.getItem() != null && e.getItem().equals(Items.Compass(Messages.ITEM_TELEPORTER.forPlayer(p))) && mp.inSpectate()) {
			try {
				final Inventory inv = this.createInv(p);
				p.openInventory(inv);
			} catch (final Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		final Player p = (Player) e.getWhoClicked();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing() && mp.inSpectate() && e.getInventory().getTitle().equals(this.INV_TITLE)) {
			if (e.getCurrentItem() == null) {
				e.setCancelled(true);
				p.closeInventory();
				return;
			}
			if (e.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {
				final String name = e.getCurrentItem().getItemMeta().getDisplayName().substring(2, e.getCurrentItem().getItemMeta().getDisplayName().trim().lastIndexOf("§") - 6).trim();

				final Player target = Murder.getInstance().getServer().getPlayerExact(name);
				if (target != null) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 255, false));
					p.teleport(target.getLocation().add(0.0D, .5D, 0.0D));
				}
				e.setCancelled(true);
				p.closeInventory();
				return;
			}
		}
	}

	private Inventory createInv(Player p) {
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Arena arena = mp.getArena();
		final int amount = arena.getAlivePlayerAmount();
		final Inventory inv = Murder.getInstance().getServer().createInventory(p, amount <= 9 ? 9 : amount <= 18 ? 18 : amount <= 27 ? 27 : 36, this.INV_TITLE);
		int slot = 0;
		for (int i = 0; i < amount; i++) {
			if (arena.getAlivePlayers().get(i) != null) {
				final MurderPlayer mp1 = MurderPlayer.getPlayer(arena.getAlivePlayers().get(i));
				if (!mp1.inSpectate()) {
					try {
						final ItemStack head = this.head(arena.getAlivePlayers().get(i));
						if (p.hasPermission(Permissions.ADMIN.perm())) {
							final ItemMeta meta = head.getItemMeta();
							final List<String> lore = head.getItemMeta().getLore();
							if (MurderPlayer.getPlayer(arena.getAlivePlayers().get(i)).isMurderer()) {
								lore.add(" ");
								lore.add("§7This Player is the §cMurderer§7." + (mp.isMurderer() && mp.disguisedTag != null ? " §r(Disguised as " + mp.disguisedTag + "§r)" : ""));
								lore.add("§8(This Message is only visible to Admins.)");
								meta.setLore(lore);
								head.setItemMeta(meta);
							}
							if (MurderPlayer.getPlayer(arena.getAlivePlayers().get(i)).isWeaponBystander()) {
								lore.add(" ");
								lore.add("§7This Player is a §9Bystander with a Secret Weapon§7.");
								lore.add("§8(This Message is only visible to Admins.)");
								meta.setLore(lore);
								head.setItemMeta(meta);
							}
						}
						inv.setItem(slot, head);
						slot++;
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return inv;
	}

	private ItemStack head(Player p) {
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		final SkullMeta meta = (SkullMeta) is.getItemMeta();
		meta.setDisplayName("§9" + p.getName() + " §7/§r" + mp.getNameTag());
		meta.setOwner(p.getName());
		meta.setLore(faces.get(p) != null ? Arrays.asList(faces.get(p)) : new ArrayList<String>());
		is.setItemMeta(meta);
		return is;
	}

	public static void addFace(Player p) {
		final PixelImgTask task = new PixelImgTask(p);
		task.start();
	}

}
