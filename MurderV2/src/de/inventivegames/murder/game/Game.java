/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.game;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.Signs;
import de.inventivegames.murder.arena.Arena;
import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.arena.ArenaStatus;
import de.inventivegames.murder.event.GameEndEvent;
import de.inventivegames.murder.event.GameStartEvent;
import de.inventivegames.murder.event.PlayerKillEvent;
import de.inventivegames.murder.game.corpse.Corpses;
import de.inventivegames.murder.misc.BossBar;
import de.inventivegames.murder.misc.Titles;
import de.inventivegames.murder.threads.PixelImgTask;
import de.inventivegames.nickname.Nicks;
import de.inventivegames.util.message.Messages;
import org.bukkit.*;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Game {

	public static void join(String name, Player p) {
		join(ArenaManager.getIDByName(name), p);
	}

	public static void join(int id, Player p) {
		final Arena arena = ArenaManager.getByID(id);
		if (arena == null) {
			p.sendMessage(Messages.ARENA_NOT_EXISTING.forPlayer(p));
			return;
		}
		if (!(arena.getPlayerAmount() <= Murder.maxPlayers)) {
			p.sendMessage(Messages.ARENA_FULL.forPlayer(p));
			arena.setStatus(ArenaStatus.FULL);
			return;
		}
		if (!arena.spawnpointExists(SpawnType.LOBBY)) {
			p.sendMessage(Messages.LOBBY_SPAWN_NOT_EXISTING.forPlayer(p));
			return;
		}

		arena.join(p);
	}

	public static void leave(int id, Player p, boolean broadcast, boolean v) {
		final Arena arena = ArenaManager.getByID(id);

		arena.leave(p, broadcast, v);
	}

	// ////////////////////////////////////////////////////////////////

	private final Arena arena;
	@SuppressWarnings("unused")
	private boolean started = false;
	// Schedulers
	private int delayedStart;
	private int countdown;
	private int countdownLobby;
	public  int smokeDelay;
	public boolean stopping = false;
	public boolean stopped  = false;
	private int loot;
	private int cd0;
	private int cd1;
	private int cd2;
	private int cd3;
	@SuppressWarnings("unused")
	private int cd4;

	public Game(Arena arena) {
		this.arena = arena;
	}

	public void cancelAllTaks() {
		this.cancelTaks(this.delayedStart, this.countdown, this.countdownLobby, this.smokeDelay, this.loot, this.cd0, this.cd1, this.cd2, this.cd3, this.arena.knifeTimer, this.arena.knifeTimer, this.arena.reloadTimer, this.arena.speedTimer);
	}

	private void cancelTaks(int... integers) {
		for (final int i : integers) {
			Bukkit.getScheduler().cancelTask(i);
		}
	}

	public boolean gameWon = false;

	@SuppressWarnings("deprecation")
	public void onPlayerDeath(PlayerDeathEvent e) {
		if (e.getEntity() instanceof Player) {
			final Player p = e.getEntity();
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.inGame()) {
				e.setDeathMessage(null);

				mp.setInSpectate();
				p.setHealth(20);
				e.getDrops().clear();
				p.teleport(e.getEntity().getLocation());

				Bukkit.getScheduler().runTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {
						try {
							Corpses.spawnCorpse(p.getLocation(), p);
						} catch (final Exception e2) {
							e2.printStackTrace();
						}
					}
				});

				for (double d = 0.0; d < 2.0; d += 0.5) {
					e.getEntity().getLocation().getWorld().playEffect(e.getEntity().getLocation().add(0D, d, 0D), Effect.STEP_SOUND, Material.REDSTONE_BLOCK.getId());
				}

				p.getInventory().setHeldItemSlot(0);

				p.setLevel(0);
				if (p.getLevel() != 0 && p.getLevel() < 5) {
					for (int i = 0; i < p.getLevel(); i++) {
						final Item loot = p.getWorld().dropItemNaturally(p.getLocation(), Items.Loot(Messages.ITEM_LOOT.forPlayer(p)));
						this.arena.addItem(loot);
					}
				}
				if (e.getEntity().getKiller() instanceof Player) {
					String prev = mp.getNameTag();
					mp.setNameTag("");
					mp.nameTag = prev;

					final Player killer = e.getEntity().getKiller();
					final MurderPlayer mpKiller = MurderPlayer.getPlayer(killer);
					Murder.getConsole().sendMessage(Murder.getDebugPrefix() + mpKiller + " killed " + mp);

					p.sendMessage(Messages.SPECTATOR.forPlayer(p));

					for (final Player online : Murder.getInstance().getServer().getOnlinePlayers()) {
						if (online.isOnline() && p != online) {
							online.hidePlayer(Bukkit.getPlayer(p.getUniqueId()));
						}
					}
					Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
						@Override
						public void run() {
							p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 255, false));

							p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 2147483647, 255, false));
						}
					}, 5L);

					Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
						@Override
						public void run() {
							p.getInventory().setItem(0, Items.Compass(Messages.ITEM_TELEPORTER.forPlayer(p)));
						}
					}, 20L);
					if (mpKiller.isMurderer()) {

						try {
							if (this.arena.getWeaponBystanders().contains(mp)) {
								this.arena.removeWeaponBystander(mp);
							}
						} catch (final Exception e1) {
						}
						Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "BystanderAmount(Death) " + this.arena.getBystanderAmount());
						if (this.arena.getBystanderAmount() <= 0) {
							if (this.gameWon) { return; }
							this.arena.sendMessage(Messages.WIN_MURDERER_A, new Object[0]);
							this.arena.sendMessage(Messages.WIN_MURDERER_INFO, mpKiller.getNameTag(), mpKiller.getNameTag().substring(0, 2) + killer.getName());

							this.gameWon = true;

							Murder.getInstance().getServer().getPluginManager().callEvent(new PlayerKillEvent(this.arena, true, "murderer", killer, p, false));

							this.arena.game.stopDelayed(200L);

						}

						if (p.getInventory().contains(Items.Gun(Messages.ITEM_GUN.forPlayer(p)))) {
							Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
								@Override
								public void run() {
									final Item gun = p.getWorld().dropItemNaturally(p.getLocation(), Items.Gun(Messages.ITEM_GUN.name()));
									Game.this.arena.addItem(gun);
								}
							}, 2L);
						}
					} else if (mpKiller.isWeaponBystander()) {
						if (mp.isBystander()) {

							this.arena.sendMessage(Messages.KILL_INNOCENT, killer.getName());
							killer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2147000, 1, false));
							killer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 2147000, 1, false));

							Murder.getInstance().getServer().getPluginManager().callEvent(new PlayerKillEvent(this.arena, false, "bystander", killer, p, true));

							mpKiller.cantPickup(true);
							mpKiller.setWeaponBystander(false);
							this.arena.removeWeaponBystander(mpKiller);
							mpKiller.setBystander(true);

							final ItemStack nameTag = killer.getInventory().getItem(0);

							Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
								@Override
								public void run() {
									killer.getInventory().remove(Items.Gun(Messages.ITEM_GUN.forPlayer(killer)).getType());
									killer.getInventory().remove(Items.Gun(Messages.ITEM_GUN.forPlayer(killer)));
									killer.getInventory().setItem(4, null);
									killer.getItemInHand().setType(Material.AIR);
									killer.getInventory().getItemInHand().setType(Material.AIR);
									killer.getInventory().getItemInHand().setAmount(0);
									killer.updateInventory();

									killer.getInventory().setItem(0, nameTag);
								}
							}, 1L);

							Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
								@Override
								public void run() {
									final Item gun = killer.getWorld().dropItemNaturally(killer.getLocation(), Items.Gun(Messages.ITEM_GUN.name()));
									Game.this.arena.addItem(gun);
								}
							}, 2L);

							killer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2147000, 1, false));
							killer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 2147000, 1, false));

							Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
								@Override
								public void run() {
									final Player pl = killer;
									for (final PotionEffect effect : pl.getActivePotionEffects()) {
										pl.removePotionEffect(effect.getType());
									}
								}
							}, 200L);

							Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
								@Override
								public void run() {
									mpKiller.cantPickup(false);

								}
							}, 20 * 20);
						} else if (mp.isMurderer()) {
							if (this.gameWon) { return; }
							this.arena.sendMessage(Messages.KILL_MURDERER, killer.getName());
							this.arena.sendMessage(Messages.WIN_BYSTANDER_A, new Object[0]);
							this.arena.sendMessage(Messages.WIN_MURDERER_INFO, mp.getNameTag(), p.getName());
							this.gameWon = true;

							Murder.getInstance().getServer().getPluginManager().callEvent(new PlayerKillEvent(this.arena, true, "bystander", killer, p, false));

							this.arena.game.stopDelayed(200L);
						}
					}

					Murder.getInstance().getServer().getPluginManager().callEvent(new de.inventivegames.murder.event.PlayerDeathEvent(p, this.arena, killer));
				} else if (e.getEntity().getKiller() instanceof Arrow) {
					this.arena.despawnKnifes();
					final Item item = p.getLocation().getWorld().dropItemNaturally(p.getLocation().add(0.0D, 1.0D, 0.0D), Items.Knife(Messages.ITEM_KNIFE.name()));
					this.arena.addItem(item);
				}

			}
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			p.updateInventory();
		}

	}

	public void startDelayed() {
		this.broadcastLobbyCountdown();
		this.arena.starting(true);
		this.started = true;
		if (this.arena.getPlayerAmount() >= Murder.minPlayers) {
			if (!Bukkit.getScheduler().isCurrentlyRunning(this.delayedStart)) {
				if (Bukkit.getPluginManager().isPluginEnabled("ResourcePackApi")) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

						@Override
						public void run() {
							for (Player p : Game.this.arena.getPlayers()) {
								MurderPlayer mp = MurderPlayer.getPlayer(p);
								if (mp.playing()) {
									if (!mp.downloadedRP) {
										p.sendMessage(Messages.RESOURCEPACK_DECLINED.forPlayer(p));
										mp.leaveArena(true);
										mp.downloadedRP = false;
										mp.downloadingRP = false;
									}
								}
							}
						}
					}, 20 * Murder.lobbyCountdown / 2);
				}
				this.delayedStart = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {
						if (Game.this.arena.getPlayerAmount() >= Murder.minPlayers) {
							Game.this.start();
						} else {
							Game.this.arena.started = false;
						}

						Game.this.arena.starting(false);
						Bukkit.getScheduler().cancelTask(Game.this.delayedStart);
						Game.this.delayedStart = -1;
					}

				}, 20 * Murder.lobbyCountdown);
			}
		}
	}

	public void start() {
		if (this.arena.getAlivePlayerAmount() < 2) { return; }
		Bukkit.getScheduler().runTaskAsynchronously(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				Game.this.arena.updatePlayerList();
				Game.this.arena.despawnAllArrows();
				Game.this.arena.despawnKnifes();
				Game.this.arena.despawnAllItems();
			}
		});

		this.started = true;

		this.arena.inGame(true);

		this.arena.setStatus(ArenaStatus.INGAME);
		for (final Player p : this.arena.getPlayers()) {
			p.getWorld().setDifficulty(Difficulty.NORMAL);
			p.setGameMode(GameMode.ADVENTURE);
			p.getInventory().clear();
			p.updateInventory();
		}

		this.arena.addEffects();
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				Game.this.assign();
			}
		}, 5L);
		this.broadcastCountdown();

		if (!Bukkit.getScheduler().isCurrentlyRunning(this.cd0)) {
			this.cd0 = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					Game.this.teleport();
					Bukkit.getScheduler().cancelTask(Game.this.cd0);
				}
			}, 20 * (Murder.countdown / 2) + 1);
		}
		if (!Bukkit.getScheduler().isCurrentlyRunning(this.cd1)) {
			this.cd1 = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					Game.this.giveItems();
					for (final Player p : Game.this.arena.getPlayers()) {
						final MurderPlayer mp = MurderPlayer.getPlayer(p);
						mp.removeEffects();

						Bukkit.getScheduler().runTaskAsynchronously(Murder.getInstance(), new Runnable() {

							@Override
							public void run() {
								try {
									Titles.sendIdentityTitle(mp);
								} catch (Exception e) {
									e.printStackTrace();
								}
								try {
									BossBar.sendObjective(mp);
								} catch (Exception e) {
									e.printStackTrace();
								}

								PixelImgTask.cancelThread(p);
							}
						});

						p.setExp(0.0f);
						p.setLevel(0);

						if (mp.isBystander()) {
							p.setFoodLevel(6);
						}

						for (Player p1 : Game.this.arena.getPlayers()) {
							if (p1 != p) {
								p1.showPlayer(p);
							}
						}

					}

					Game.this.startSmoke();

					Bukkit.getScheduler().cancelTask(Game.this.countdown);
					Bukkit.getScheduler().cancelTask(Game.this.countdownLobby);
					Bukkit.getScheduler().cancelTask(Game.this.delayedStart);

					Bukkit.getScheduler().cancelTask(Game.this.cd1);
				}
			}, 20 * Murder.countdown);
		}

		if (!Bukkit.getScheduler().isCurrentlyRunning(this.cd2)) {
			this.cd2 = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					Bukkit.getScheduler().cancelTask(Game.this.countdown);
					Bukkit.getScheduler().cancelTask(Game.this.countdownLobby);
					Bukkit.getScheduler().cancelTask(Game.this.delayedStart);

					Bukkit.getScheduler().cancelTask(Game.this.cd2);
				}
			}, 20 * Murder.countdown + 50);
		}

		if (!Bukkit.getScheduler().isCurrentlyRunning(this.cd3)) {
			this.cd3 = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					Game.this.spawnLoot();

					Bukkit.getScheduler().cancelTask(Game.this.countdown);
					Bukkit.getScheduler().cancelTask(Game.this.countdownLobby);
					Bukkit.getScheduler().cancelTask(Game.this.delayedStart);

					Bukkit.getScheduler().cancelTask(Game.this.cd3);
				}
			}, 20 * Murder.countdown + 100);
		}

		Murder.getInstance().getServer().getPluginManager().callEvent(new GameStartEvent(this.arena));
		Signs.updateSigns(this.arena.getID());
	}

	public void stop() {
		if (this.stopped) { return; }
		Bukkit.getScheduler().cancelTask(this.loot);
		Bukkit.getScheduler().cancelTask(this.arena.knifeTimer);
		Bukkit.getScheduler().cancelTask(this.arena.reloadTimer);

		Bukkit.getScheduler().runTaskAsynchronously(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				Corpses.despawnCorpse(Game.this.arena);
			}
		});

		String winner = "murderer";

		this.arena.started = false;

		final List<Player> players = new ArrayList<Player>(this.arena.getPlayers());

		for (Player p : this.arena.getPlayers()) {
			if (p.getPassenger() != null) {
				p.getPassenger().eject();
			}
			p.setPassenger(null);
			MurderPlayer.getPlayer(p).setNameTag("");
		}

		if (this.arena.getBystanderAmount() == 0) {
			winner = "murderer";
		} else {
			winner = "bystanders";
		}
		Murder.getInstance().getServer().getPluginManager().callEvent(new GameEndEvent(this.arena, players, winner));

		Bukkit.getScheduler().runTaskAsynchronously(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				for (final Item item : Game.this.arena.getItems()) {
					if (item != null) {
						item.remove();
					}
				}

				Game.this.despawnLoot();
				Game.this.arena.despawnAllItems();
				Game.this.arena.despawnAllArrows();

				for (final Player p : players) {
					final MurderPlayer mp = MurderPlayer.getPlayer(p);
					Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

						@Override
						public void run() {
							mp.leaveArena(false, false);
							mp.removeEffects();
							Nicks.updatePlayer(p);
						}
					});
					mp.setNameTag(null);
				}
			}
		});

		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				Game.this.arena.reset();
			}
		}, 1);

		this.stopped = true;

		this.arena.setStatus(ArenaStatus.WAITING);
		Signs.updateSigns(this.arena.getID());

		this.arena.updatePlayerList();
	}

	public void stopDelayed(long l) {
		if (this.stopping) { return; }
		this.arena.setStatus(ArenaStatus.END);
		this.printScoreboard();
		this.stopping = true;
		Bukkit.getScheduler().cancelTask(this.loot);
		Bukkit.getScheduler().cancelTask(this.arena.knifeTimer);
		Bukkit.getScheduler().cancelTask(this.arena.reloadTimer);
		this.cancelAllTaks();
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
			@Override
			public void run() {
				Game.this.stop();
			}
		}, l);
	}

	public void broadcastLobbyCountdown() {

		if (!Bukkit.getScheduler().isCurrentlyRunning(this.countdownLobby)) {
			this.countdownLobby = Bukkit.getScheduler().scheduleSyncRepeatingTask(Murder.getInstance(), new Runnable() {
				int time = Murder.lobbyCountdown;

				@Override
				public void run() {
					for (Player p : Game.this.arena.getPlayers()) {
						p.setLevel(this.time);
						p.setExp((float) this.time / (float) Murder.lobbyCountdown);
					}
					if (this.time == 30 || this.time == 20 || this.time == 10 || this.time < 6) {
						Game.this.arena.sendMessage(Messages.COUNTDOWN_LOBBY, this.time);
					}
					if (this.time <= 1) {
						Bukkit.getScheduler().cancelTask(Game.this.countdownLobby);
					}
					this.time--;
				}
			}, 0L, 20L);
		}
	}

	public void broadcastCountdown() {
		Bukkit.getScheduler().cancelTask(this.countdownLobby);
		if (!Bukkit.getScheduler().isCurrentlyRunning(this.countdown)) {
			this.countdown = Bukkit.getScheduler().scheduleSyncRepeatingTask(Murder.getInstance(), new Runnable() {
				int time = Murder.countdown;

				@Override
				public void run() {
					for (Player p : Game.this.arena.getPlayers()) {
						p.setLevel(this.time);
						p.setExp((float) this.time / (float) Murder.countdown);
					}
					if (this.time == 30 || this.time == 20 || this.time == 10 || this.time < 6) {
						Game.this.arena.sendMessage(Messages.COUNTDOWN_GAME, this.time);
					}
					if (this.time <= 1) {
						Bukkit.getScheduler().cancelTask(Game.this.countdown);
					}
					this.time--;
				}
			}, 0L, 20L);
		}
	}

	public void assign() {
		Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "=== Assign (" + this.arena.getID() + ") ===");
		final List<Player> temp = new ArrayList<Player>();
		for (final Player p : this.arena.getPlayers()) {
			if (!this.arena.getForcedMurderers().contains(p) && !this.arena.getForcedWeaponBystanders().contains(p)) {
				if (this.arena.getPlayerAmount() > 3) {
					if (!Murder.murdererBlacklist.contains(p) && !Murder.weaponBlacklist.contains(p)) {
						temp.add(p);
					} else {
						Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Ignoring " + p.getName() + ", Reason: " + (Murder.murdererBlacklist.contains(p) ? "MurdererBlacklist" : Murder.weaponBlacklist.contains(p) ? "WeaponBystanderBlacklist" : "NONE"));
					}
				} else {
					temp.add(p);
				}
			}
		}

		Player p;
		MurderPlayer mp;

		// /////////

		if (this.arena.getForcedMurderers().isEmpty()) {
			p = temp.get(Murder.rd.nextInt(temp.size()));
			mp = MurderPlayer.getPlayer(p);

			mp.setMurderer(true);
			this.arena.setMurderer(mp);

			p.sendMessage(Messages.MURDERER.forPlayer(p));
			try {
				Titles.sendRoleTitle(mp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Murderer: " + p.getName());
			Murder.murdererBlacklist.add(p);
			Murder.weaponBlacklist.remove(p);

			temp.remove(p);
		} else {
			p = this.arena.getForcedMurderers().get(0);
			mp = MurderPlayer.getPlayer(p);

			mp.setMurderer(true);
			this.arena.setMurderer(mp);

			p.sendMessage(Messages.MURDERER.forPlayer(p));
			try {
				Titles.sendRoleTitle(mp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Murderer(Forced): " + p.getName());
			Murder.murdererBlacklist.add(p);
			Murder.weaponBlacklist.remove(p);

			Murder.forcedMurderers.remove(p);
			temp.remove(p);
		}

		// ///////////

		if (this.arena.getForcedWeaponBystanders().isEmpty()) {
			p = temp.get(Murder.rd.nextInt(temp.size()));
			mp = MurderPlayer.getPlayer(p);

			mp.setWeaponBystander(true);
			this.arena.addWeaponBystander(mp);

			p.sendMessage(Messages.BYSTANDER_WEAPON.forPlayer(p));
			try {
				Titles.sendRoleTitle(mp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "WeaponBystander: " + p.getName());
			Murder.weaponBlacklist.add(p);
			Murder.murdererBlacklist.remove(p);

			temp.remove(p);
		} else {
			p = this.arena.getForcedWeaponBystanders().get(0);
			mp = MurderPlayer.getPlayer(p);

			mp.setWeaponBystander(true);
			this.arena.addWeaponBystander(mp);

			p.sendMessage(Messages.BYSTANDER_WEAPON.forPlayer(p));
			try {
				Titles.sendRoleTitle(mp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "WeaponBystander(Forced): " + p.getName());
			Murder.weaponBlacklist.add(p);
			Murder.murdererBlacklist.remove(p);

			Murder.forcedWeapons.remove(p);
			temp.remove(p);
		}

		for (final Player pl : temp) {
			p = pl;
			mp = MurderPlayer.getPlayer(p);

			mp.setBystander(true);

			p.sendMessage(Messages.BYSTANDER.forPlayer(p));
			try {
				Titles.sendRoleTitle(mp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Bystander(Default): " + p.getName());
			Murder.murdererBlacklist.remove(p);
			Murder.weaponBlacklist.remove(p);
		}

		// //////////////

		final List<Player> murdererBlacklist = new ArrayList<Player>(this.arena.getMurdererBlacklist());
		for (final Player pl : murdererBlacklist) {
			p = pl;
			mp = MurderPlayer.getPlayer(p);
			if (this.arena.getPlayerAmount() >= 3) {

				if (!mp.hasRole()) {
					mp.setBystander(true);
					p.sendMessage(Messages.BYSTANDER.forPlayer(p));
					try {
						Titles.sendRoleTitle(mp);
					} catch (Exception e) {
						e.printStackTrace();
					}
					Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Bystander(MB): " + p.getName());
					Murder.murdererBlacklist.remove(p);
				}

			} else if (!mp.hasRole()) {
				mp.setBystander(true);
				p.sendMessage(Messages.BYSTANDER.forPlayer(p));
				try {
					Titles.sendRoleTitle(mp);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Bystander(MB): " + p.getName());
				Murder.murdererBlacklist.remove(p);
			}

		}
		final List<Player> weaponBlacklist = new ArrayList<Player>(this.arena.getWeaponBlacklist());
		for (final Player pl : weaponBlacklist) {
			p = pl;
			mp = MurderPlayer.getPlayer(p);
			if (this.arena.getPlayerAmount() >= 3) {
				if (!mp.hasRole()) {
					mp.setBystander(true);
					p.sendMessage(Messages.BYSTANDER.forPlayer(p));
					try {
						Titles.sendRoleTitle(mp);
					} catch (Exception e) {
						e.printStackTrace();
					}
					Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Bystander(WB): " + p.getName());
					Murder.weaponBlacklist.remove(p);
				}

			} else if (!mp.hasRole()) {
				mp.setBystander(true);
				p.sendMessage(Messages.BYSTANDER.forPlayer(p));
				try {
					Titles.sendRoleTitle(mp);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "Bystander(WB): " + p.getName());
				Murder.weaponBlacklist.remove(p);
			}
		}

		temp.clear();

		this.disguisePlayers();

		Signs.updateSigns(this.arena.getID());
		Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "===================");
	}

	public void forceMurderer(Player p) {

	}

	public void forceWeaponBystander(Player p) {

	}

	public void giveItems() {
		for (final Player p : this.arena.getPlayers()) {
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + p.getName() + " " + mp.getNameTag() + " " + (mp.isWeaponBystander() ? "WB" : mp.isMurderer() ? "M" : "B"));
			if (mp.playing()) {
				p.getInventory().setItem(0, Items.NameInfo(p));
				if (mp.isMurderer()) {
					p.getInventory().setItem(4, Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)));
				} else if (mp.isWeaponBystander()) {
					p.getInventory().setItem(4, Items.Gun(Messages.ITEM_GUN.forPlayer(p)));
					p.getInventory().setItem(8, Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)));
				} else if (mp.isBystander()) {
					p.getInventory().setItem(8, Items.SpeedBoost(Messages.ITEM_SPEED.forPlayer(p)));
				} else {
					Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§cUnexpected error in giveItems(): Player " + mp + " has no role!");
				}
				p.updateInventory();
				mp.setInGame();
			}
		}
	}

	public void teleport() {
		final ArrayList<Spawnpoint> points = (ArrayList<Spawnpoint>) this.arena.getSpawnpoint(SpawnType.PLAYERS);
		for (int i = 0; i < this.arena.getPlayerAmount(); i++) {
			final Player p = this.arena.getPlayers().get(i);
			Location loc = points.get(i).toLocation();
			if (loc.getX() == 0.0D || loc.getY() == 0.0D || loc.getZ() == 0.0D) {
				i = 0;
				loc = points.get(i).toLocation();
			}
			p.teleport(loc);
			p.getInventory().setHeldItemSlot(0);
		}
	}

	public void printScoreboard() {
		for (final Player p : this.arena.getPlayers()) {
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			final String playerName = p.getName();
			final String nameTag = mp.getNameTag();
			if (nameTag != null && nameTag != "§cNULL" && nameTag.length() > 2) {
				final String tagColor = nameTag.substring(0, 2);
				final int collectedLoot = p.getLevel();

				String nameSpace = "";
				for (int x = 0; x < 18 - playerName.length(); x++) {
					nameSpace = nameSpace + " ";
				}
				String tagSpace = "";
				for (int x = 0; x < 18 - nameTag.length(); x++) {
					tagSpace = tagSpace + " ";
				}
				final String score = "            " + tagColor + playerName + nameSpace + " §7|§r " + tagColor + nameTag + tagSpace + " §7|§r " + tagColor + collectedLoot;

				this.arena.sendMessage(score);
			}
		}
	}

	public void disguisePlayers() {
		for (final Player p : this.arena.getPlayers()) {
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			final int Name = new Random().nextInt(Murder.getNameTags().length - 1);
			final int Color = new Random().nextInt(Murder.getColorCode().length - 1);

			final String color = Murder.getColorCode()[Color];
			final String name = Murder.getNameTags()[Name];

			if (p != null && color != null && name != null) {
				mp.setNameTag(color + name);
			}
		}
	}

	private double randomTime = Murder.rd.nextInt(55) + 15;

	public void spawnLoot() {

		if (!Bukkit.getScheduler().isCurrentlyRunning(this.loot)) {
			this.loot = Bukkit.getScheduler().scheduleSyncRepeatingTask(Murder.getInstance(), new Runnable() {
				int Number = 1;

				@Override
				public void run() {

					final List<Spawnpoint> points = Game.this.arena.getSpawnpoint(SpawnType.LOOT);
					if (points == null) { return; }
					if (this.Number >= points.size()) {
						this.Number = 1;
					}

					final Location loc = points.get(this.Number).toLocation();
					final World world = loc.getWorld();
					Game.this.arena.addLoot(world.dropItemNaturally(loc, Items.Loot(Messages.ITEM_LOOT.name())));
					Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§7Spawning Loot in Arena " + Game.this.arena.getID() + " at " + loc);
					this.Number++;
					Game.this.randomTime = Murder.rd.nextInt(Murder.max_loot_delay) + Murder.min_loot_delay;
				}
			}, (int) this.randomTime * 20, (int) this.randomTime * 20);
		}
	}

	public void despawnLoot() {
		Bukkit.getScheduler().cancelTask(this.loot);
		for (final Item item : this.arena.getLoot()) {
			item.remove();
		}
	}

	public void startSmoke() {
		if (Murder.smokeTimer != -1) {
			if (!Bukkit.getScheduler().isCurrentlyRunning(this.smokeDelay)) {
				this.smokeDelay = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
					@Override
					public void run() {
						Game.this.arena.smoke = true;
						if (Game.this.arena.getMurderer() != null) {
							Game.this.arena.getMurderer().player().sendMessage(Messages.NOTIFICATON_SMOKE.forPlayer(Game.this.arena.getMurderer().player()));
						}
					}
				}, 20 * Murder.smokeTimer);
			}
		}
	}

	public void cancelDelayedStart() {
		Bukkit.getScheduler().cancelTask(this.delayedStart);
		this.arena.starting(false);
		this.started = false;
		Bukkit.getScheduler().cancelTask(this.countdownLobby);
	}

}
