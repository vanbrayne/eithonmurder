/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.game.corpse;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.SkinStorage;
import de.inventivegames.murder.arena.Arena;
import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.nickname.AccessUtil;
import de.inventivegames.nickname.SkinLoader;
import de.inventivegames.npc.living.NPCPlayer;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import org.json.simple.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Corpses implements Listener {

	private static final Map<Arena, List<de.inventivegames.npc.NPC>> npcMap = new HashMap<>();

	public static void spawnCorpse(Location loc, Player p) throws Exception {
		if (!Bukkit.getPluginManager().isPluginEnabled("NPCLib")) { return; }
		final MurderPlayer mp = MurderPlayer.getPlayer(p);

		JSONObject obj = SkinLoader.getSkin(SkinStorage.get(mp.getNameTag().substring(1, 2) + "b"));
		String skin = obj.toString();

		Field f = AccessUtil.setAccessible(de.inventivegames.npc.NPCLibPlugin.class.getDeclaredField("useNickNamer"));
		boolean prev = f.getBoolean(null);
		f.set(null, false);// Disable automatic skins from NickNamer since we'll inject our own data

		final de.inventivegames.npc.living.NPCPlayer npc = (NPCPlayer) de.inventivegames.npc.NPCLib.spawnPlayerNPC(loc, mp.getNameTag() + "§b", !skin.isEmpty() ? skin : null);

		f.set(null, prev);// Set it back to the old value after the NPC is spawned

		npc.setCollision(true);
		npc.setControllable(false);
		npc.setFrozen(true);
		npc.setGravity(false);
		npc.setInvulnerable(true);

		new BukkitRunnable() {

			@Override
			public void run() {
				npc.setLying(true);
			}
		}.runTaskLaterAsynchronously(Murder.getInstance(), 5);

		if (!npcMap.containsKey(mp.getArena())) {
			npcMap.put(mp.getArena(), new ArrayList<de.inventivegames.npc.NPC>());
		}
		npcMap.get(mp.getArena()).add(npc);
	}

	public static void despawnCorpse(Arena arena) {
		if (!Bukkit.getPluginManager().isPluginEnabled("NPCLib")) { return; }
		List<de.inventivegames.npc.NPC> list = npcMap.get(arena);
		if (list == null) { return; }
		for (de.inventivegames.npc.NPC npc : list) {
			npc.despawn();
		}
		list.clear();
		npcMap.remove(arena);
	}

	private static void disguiseMurderer(Player p, String name) {
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (p.getLevel() > 0) {
			mp.disguiseMurderer(name);
			mp.player().setLevel(mp.player().getLevel() - 1);
		} else {
			p.sendMessage(String.format(Messages.NOTIFICATION_LOOT.forPlayer(p), new Object[0]));
		}
	}

	@EventHandler
	public void onCollide(de.inventivegames.npc.event.NPCMotionEvent e) {//TODO update to NPCCollideEvent
		de.inventivegames.npc.NPC npc = e.getNPC();
		World world = npc.getLocation().getWorld();
		Arena arena = ArenaManager.getByWorld(world);
		if (arena == null) { return; }
		final MurderPlayer murderer = arena.getMurderer();
		if (murderer.disguiseTimeout) { return; }
		if (murderer.inSpectate()) { return; }
		if (!npcMap.get(arena).contains(npc)) { return; }
		List<Entity> nearby = murderer.player().getNearbyEntities(1, 1, 1);
		for (Entity ent : nearby) {
			if (de.inventivegames.npc.NPCLib.isNPC(ent)) {
				if (de.inventivegames.npc.NPCLib.getNPC(ent).equals(npc)) {
					murderer.player().sendMessage(Messages.NOTIFICATION_DISGUISE.forPlayer(murderer.player()));
					murderer.disguiseTimeout = true;
					Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
						@Override
						public void run() {
							murderer.disguiseTimeout = false;
						}
					}, 40L);
					break;
				}
			}
		}
	}

	@EventHandler
	public void onInteract(de.inventivegames.npc.event.NPCInteractEvent e) {
		de.inventivegames.npc.NPC npc = e.getNPC();
		Player p = e.getPlayer();
		MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing()) {
			Arena arena = mp.getArena();
			if (arena == null) { return; }
			if (!npcMap.get(arena).contains(npc)) { return; }
			if (mp.isMurderer()) {
				disguiseMurderer(p, npc.getName().substring(0, npc.getName().length() - 2));
			}
		}
	}

}
