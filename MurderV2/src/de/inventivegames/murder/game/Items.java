/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.game;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Items {

	public static ItemStack Knife(String name) {
		final ItemStack Knife = new ItemStack(Material.DIAMOND_AXE);
		final ItemMeta knifeMeta = Knife.getItemMeta();
		knifeMeta.setDisplayName("§c§l" + name);
		knifeMeta.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
		Knife.setItemMeta(knifeMeta);

		return Knife;
	}

	public static ItemStack Gun(String name) {
		final ItemStack Gun = new ItemStack(Material.DIAMOND_HOE);
		final ItemMeta gunMeta = Gun.getItemMeta();
		gunMeta.setDisplayName("§1§l" + name);
		Gun.setItemMeta(gunMeta);

		return Gun;
	}

	public static ItemStack Bullet(String name) {
		final ItemStack Bullet = new ItemStack(Material.ARROW);
		final ItemMeta bulletMeta = Bullet.getItemMeta();
		bulletMeta.setDisplayName("§8" + name);
		Bullet.setItemMeta(bulletMeta);

		return Bullet;
	}

	public static ItemStack Loot(String name) {
		final ItemStack Loot = new ItemStack(Material.DIAMOND);
		final ItemMeta lootMeta = Loot.getItemMeta();
		lootMeta.setDisplayName("§6" + name);
		lootMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		Loot.setItemMeta(lootMeta);

		return Loot;
	}

	public static ItemStack NameInfo(Player p) {
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final ItemStack Loot = new ItemStack(Material.NAME_TAG);
		final ItemMeta lootMeta = Loot.getItemMeta();
		lootMeta.setDisplayName("§l" + (mp.getNameTag() != null ? (String) mp.getNameTag() : "§r§cUnable to get NameTag!"));
		lootMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		Loot.setItemMeta(lootMeta);

		return Loot;
	}

	public static ItemStack SpeedBoost(String name) {
		final ItemStack sb = new ItemStack(Material.SUGAR);
		final ItemMeta sbMeta = sb.getItemMeta();
		sbMeta.setDisplayName("§7" + name);
		sb.setItemMeta(sbMeta);

		return sb;
	}

	public static ItemStack Compass(String name) {
		final ItemStack c = new ItemStack(Material.COMPASS);
		final ItemMeta cMeta = c.getItemMeta();
		cMeta.setDisplayName("§7" + name);
		c.setItemMeta(cMeta);

		return c;
	}
}
