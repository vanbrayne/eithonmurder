/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.InputSupplier;
import de.inventivegames.nickname.AccessUtil;
import de.inventivegames.nickname.SkinLoader;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
@SuppressWarnings("deprecation")
public class SkinStorage {

	private static Map<String, UUID> uuidMap = new HashMap<>();

	public static void load(String[] colors) throws ParseException, IOException {
		for (final String name : colors) {
			load(name.substring(1, 2));
			load(name.substring(1, 2) + "b");
		}
	}

	private static void load(final String name) throws ParseException, IOException {
		JSONObject json = (JSONObject) new JSONParser().parse(CharStreams.toString(new InputSupplier<InputStreamReader>() {
			@Override
			public InputStreamReader getInput() throws IOException {
				return new InputStreamReader(new URL(String.format("http://api.inventivetalent.org/skin/murder/?s=%s", name)).openConnection().getInputStream(), Charsets.UTF_8);
			}
		}));
		Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§8Loaded skin data for §7" + name);
		try {
			@SuppressWarnings("unchecked")
			Map<UUID, JSONObject> map = (Map<UUID, JSONObject>) AccessUtil.setAccessible(SkinLoader.class.getDeclaredField("skinStorage")).get(null);
			UUID id = UUID.randomUUID();
			map.put(id, json);// Injects the data into NickNamer
			uuidMap.put(name, id);// Stores the name for the corresponding UUID
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static UUID get(String color) {
		if (color.length() == 1) { return uuidMap.get(color); }
		if (color.length() == 2) { return uuidMap.get(color.replace("§", "")); }
		return null;
	}

	public static List<String> values() {
		List<String> list = new ArrayList<>();
		for (Entry<String, UUID> entry : uuidMap.entrySet()) {
			list.add(entry.getKey() + "=" + entry.getValue());
		}
		return list;
	}
}
