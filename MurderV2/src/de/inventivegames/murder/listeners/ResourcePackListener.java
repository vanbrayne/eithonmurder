/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.listeners;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.rpapi.ResourcePackStatusEvent;
import de.inventivegames.rpapi.Status;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * © Copyright 2015 inventivetalent
 *
 * @author inventivetalent
 */
public class ResourcePackListener implements Listener {

	@EventHandler
	public void onResourcePackStatus(ResourcePackStatusEvent e) {
		Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing()) {
			if (e.getStatus() == Status.ACCEPTED) {
				p.sendMessage(Messages.RESOURCEPACK_DOWNLOAD.forPlayer(p));
			}
			if (e.getStatus() == Status.SUCCESSFULLY_LOADED) {
				p.sendMessage(Messages.RESOURCEPACK_SUCCESS.forPlayer(p));
				mp.downloadedRP = true;
				mp.downloadingRP = false;
			}
			if (e.getStatus() == Status.FAILED_DOWNLOAD) {
				p.sendMessage(Messages.RESOURCEPACK_FAILED.forPlayer(p));
				Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {
						mp.leaveArena(true);
					}
				});
				mp.downloadedRP = false;
				mp.downloadingRP = false;
			}
			if (e.getStatus() == Status.DECLINED) {
				p.sendMessage(Messages.RESOURCEPACK_DECLINED.forPlayer(p));
				Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {
						mp.leaveArena(true);
					}
				});
				mp.downloadedRP = false;
				mp.downloadingRP = false;
			}
		}
	}

}
