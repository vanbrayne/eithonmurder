/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.listeners;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.game.Items;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.murder.misc.Reflection;
import de.inventivegames.packetlistener.handler.PacketHandler;
import de.inventivegames.packetlistener.handler.PacketOptions;
import de.inventivegames.packetlistener.handler.ReceivedPacket;
import de.inventivegames.packetlistener.handler.SentPacket;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.InvocationTargetException;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class PacketListener implements Listener {

	static Class<?> nms_item_stack;
	static Class<?> obc_craft_item_stack;
	static Class<?> nms_entity_equipment;
	static Class<?> nms_entity_human;

	protected static PacketHandler handler;

	static {
		nms_item_stack = Reflection.getNMSClass("ItemStack");
		obc_craft_item_stack = Reflection.getOBCClass("inventory.CraftItemStack");
		nms_entity_equipment = Reflection.getNMSClass("PacketPlayOutEntityEquipment");
		nms_entity_human = Reflection.getNMSClass("EntityHuman");

	}

	public PacketListener() {
		if (handler != null) {
			PacketHandler.removeHandler(handler);
			handler = null;
		}
		handler = new PacketHandler(Murder.getInstance()) {

			@Override
			@PacketOptions(forcePlayer = true)
			public void onSend(SentPacket e) {
				final Player p = e.getPlayer();
				MurderPlayer mp = MurderPlayer.getPlayer(p);
				if (e.getPacketName().equalsIgnoreCase("PacketPlayOutEntityEquipment")) {
					if (mp.playing()) {
						Entity ent = PacketListener.this.getEntityByID(p.getWorld(), (int) e.getPacketValue("a"));
						if (ent instanceof Player && MurderPlayer.getPlayer((Player) ent).playing()) {
							int slot = (int) e.getPacketValue("b");
							if (mp.inGame() && slot == 0 || mp.inSpectate()) {
								try {
									ItemStack heldItem = (ItemStack) obc_craft_item_stack.getMethod("asBukkitCopy", nms_item_stack).invoke(null, e.getPacketValue("c"));
									if (heldItem.getType() != Items.Gun(null).getType() && heldItem.getType() != Items.Knife(null).getType()) {
										ItemStack stack = new ItemStack(Material.AIR);
										Object itemStack = obc_craft_item_stack.getMethod("asNMSCopy", ItemStack.class).invoke(null, stack);
										e.setPacketValue("c", itemStack);
									}
								} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
									e1.printStackTrace();
								}
							}
						}
					}
				}
			}

			@Override
			public void onReceive(ReceivedPacket packet) {
			}
		};
		PacketHandler.addHandler(handler);
	}

	private Entity getEntityByID(World world, int id) {
		for (Entity ent : world.getEntities())
			if (ent.getEntityId() == id) { return ent; }
		return null;
	}

}
