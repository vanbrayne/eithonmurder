/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.listeners;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.game.Items;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.murder.game.Spectate;
import de.inventivegames.murder.misc.Reflection;
import de.inventivegames.murder.misc.ResourcePack;
import de.inventivegames.particle.ParticleEffect;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class PlayerListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		Team t = Bukkit.getScoreboardManager().getMainScoreboard().getPlayerTeam(p);
		if (t != null) {
			if (t.getName().startsWith(Murder.TEAM_PREFIX)) {
				t.removePlayer(p);
			}
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				if (p.isOnline()) {
					Spectate.addFace(p);
				}
			}
		}, 20L);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		final Player p = e.getPlayer();
		ResourcePack.resetResourcePack(p);
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
			@Override
			public void run() {
				if (mp.playing()) {
					mp.leaveArena(true);

					MurderPlayer.reset(mp);
				}
			}
		}, 1);
		this.onDeath(new PlayerDeathEvent(p, new ArrayList<ItemStack>(), 0, ""));
	}

	static Class<?> nms_item_stack;
	static Class<?> obc_craft_item_stack;
	static Class<?> nms_entity_equipment;

	static {
		nms_item_stack = Reflection.getNMSClass("ItemStack");
		obc_craft_item_stack = Reflection.getOBCClass("inventory.CraftItemStack");
		nms_entity_equipment = Reflection.getNMSClass("PacketPlayOutEntityEquipment");
	}

	@EventHandler
	public void onSlotChange(PlayerItemHeldEvent e) {
		Player p = e.getPlayer();
		MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing()) {
			if (mp.inGame() && e.getNewSlot() != 4 || mp.inSpectate()) {
				try {
					ItemStack stack = new ItemStack(Material.AIR);
					Object itemStack = obc_craft_item_stack.getMethod("asNMSCopy", ItemStack.class).invoke(null, stack);
					final Object packet = nms_entity_equipment.getConstructor(int.class, int.class, nms_item_stack).newInstance(Integer.valueOf(((Entity) p).getEntityId()).intValue(), 0, itemStack);
					for (Player p1 : mp.getArena().getPlayers()) {
						if (p == p1) {
							continue;
						}
						final Object handle = Reflection.getHandle(p1);
						final Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
						Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

							@Override
							public void run() {
								try {
									Reflection.getMethod(connection.getClass(), "sendPacket").invoke(connection, packet);
								} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
									e.printStackTrace();
								}
							}
						});
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	@EventHandler
	public void ItemDrop(PlayerDropItemEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final int slot = p.getInventory().getHeldItemSlot();
		final ItemStack item = e.getItemDrop().getItemStack();
		if (mp.playing()) {
			e.getItemDrop().remove();
			p.getInventory().remove(item);
			p.getInventory().setItem(slot, item);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		final Player p = (Player) e.getWhoClicked();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing()) {
			e.setCancelled(true);
			p.closeInventory();
		}
	}

	@EventHandler
	public void pickupItem(PlayerPickupItemEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Item item = e.getItem();
		if (mp.cantPickup()) {
			p.getInventory().remove(Items.Gun(Messages.ITEM_GUN.forPlayer(p)));
			p.updateInventory();
			e.setCancelled(true);
			return;
		}
		if (mp.inSpectate()) {
			e.setCancelled(true);
			return;
		}
		if (mp.playing()) {
			if (mp.isBystander()) {
				if (item.getItemStack().getType().equals(Items.Gun(Messages.ITEM_GUN.name()).getType())) {
					if (!mp.cantPickup()) {
						e.getItem().remove();
						p.getInventory().setItem(4, null);
						p.getInventory().setItem(4, Items.Gun(Messages.ITEM_GUN.forPlayer(p)));
						mp.setBystander(false);
						mp.setWeaponBystander(true);
						mp.getArena().addWeaponBystander(mp);
						if (!p.getInventory().contains(Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)).getType())) {
							p.getInventory().setItem(8, Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)));
						}
						e.setCancelled(true);
					} else {
						e.setCancelled(true);
					}
				} else {
					e.setCancelled(true);
				}
			} else if (mp.isMurderer()) {
				if (item.getItemStack().getType().equals(Items.Knife(Messages.ITEM_KNIFE.name()).getType())) {
					Bukkit.getScheduler().cancelTask(mp.getArena().trailTask);
					Bukkit.getScheduler().cancelTask(mp.getArena().knifeTimer);
					if (mp.getArena().droppedKnife != null) {
						mp.getArena().droppedKnife.remove();
					}
					mp.getArena().despawnKnifes();
					mp.getArena().droppedKnife = null;
					p.getInventory().setItem(4, null);
					p.getInventory().setItem(4, Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)));
					e.getItem().remove();
					e.setCancelled(true);
				} else {
					e.setCancelled(true);
				}
			}
			if ((mp.isBystander() || mp.isMurderer()) && item.getItemStack().getType().equals(Items.Loot(Messages.ITEM_LOOT.name()).getType())) {
				item.remove();
				if (p.getLevel() < 0) {
					p.setLevel(0);
				}
				p.setLevel(p.getLevel() + e.getItem().getItemStack().getAmount());
				p.sendMessage(String.format(Messages.LOOT_COLLECTED.forPlayer(p), e.getItem().getItemStack().getAmount()));
				if (p.getLevel() >= 5 && !mp.isMurderer()) {
					p.getInventory().setItem(4, Items.Gun(Messages.ITEM_GUN.forPlayer(p)));
					p.getInventory().setItem(8, Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)));
					mp.setWeaponBystander(true);
					mp.setBystander(false);
					mp.getArena().addWeaponBystander(mp);
				}
				e.setCancelled(true);
			}
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPaintingBreak(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (e.getEntity() instanceof ItemFrame || e.getEntity() instanceof Painting) {
				if (MurderPlayer.getPlayer(p).playing()) {
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onPaintingBreak(HangingBreakByEntityEvent e) {
		if (e.getRemover() instanceof Player) {
			Player p = (Player) e.getRemover();
			if (e.getEntity() instanceof ItemFrame || e.getEntity() instanceof Painting) {
				if (MurderPlayer.getPlayer(p).playing()) {
					e.setCancelled(true);
				}
			}
		} else {
			if (e.getEntity() instanceof ItemFrame || e.getEntity() instanceof Painting) {
				if (ArenaManager.getByWorld(e.getEntity().getWorld()) != null) {
					e.setCancelled(true);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void onDamage(EntityDamageByEntityEvent e) {
		final Entity ent = e.getEntity();

		if (ent instanceof Player) {
			if (MurderPlayer.getPlayer((Player) e.getEntity()).playing() && e.getCause() == EntityDamageEvent.DamageCause.PROJECTILE) {
				if (e.getDamager() instanceof Arrow) {
					if (((Projectile) e.getDamager()).hasMetadata("Murder")) {
						if (((Projectile) e.getDamager()).getMetadata("Murder").get(0).value().equals("ITEMS_KNIFE") || ((Projectile) e.getDamager()).getMetadata("Murder").get(0).value().equals("ITEMS_BULLET")) {
							if (MurderPlayer.getPlayer((Player) ent).inGame()) {
								if (!MurderPlayer.getPlayer((Player) ent).isMurderer()) {
									((Player) ent).setHealth(2.0D);
									e.setDamage(20.0D);
								}
							}
						}
					}
				}
			}
		}

		if (ent instanceof Player && e.getDamager() instanceof Player) {
			final Player p = (Player) e.getDamager();
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.playing()) {
				if (!p.getItemInHand().getType().equals(Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)).getType())) {
					e.setCancelled(true);
					return;
				}
				final MurderPlayer mpEnt = MurderPlayer.getPlayer((Player) ent);
				if (mp.inSpectate() || mpEnt.inSpectate()) {
					e.setCancelled(true);
					return;
				}

				for (double d = 0.0; d < 2.0; d += 0.5) {
					e.getEntity().getLocation().getWorld().playEffect(e.getEntity().getLocation().add(0D, d, 0D), Effect.STEP_SOUND, Material.REDSTONE_BLOCK.getId());
				}
			}
		}

	}

	@EventHandler
	public void onOtherDamage(EntityDamageEvent e) {
		final Entity ent = e.getEntity();
		if (ent instanceof Player) {
			final Player p = (Player) ent;
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.inSpectate() || mp.inLobby()) {
				e.setDamage(0.0D);
				e.setCancelled(true);
			}
			if (mp.playing() && (e.getCause() == EntityDamageEvent.DamageCause.FALL || e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK || e.getCause() == EntityDamageEvent.DamageCause.DROWNING || e.getCause() == EntityDamageEvent.DamageCause.SUFFOCATION)) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onDeath(org.bukkit.event.entity.PlayerDeathEvent e) {
		if (e.getEntity() instanceof Player) {
			final Player p = e.getEntity();
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.playing()) {
				mp.getArena().game.onPlayerDeath(e);
			}
		}
	}

	@EventHandler
	public void onArrowBlock(EntityDamageByEntityEvent event) {
		final Entity entityDamager = event.getDamager();
		final Entity entityDamaged = event.getEntity();
		if (entityDamager instanceof Arrow && entityDamaged instanceof Player && ((Arrow) entityDamager).getShooter() instanceof Player) {
			final Arrow arrow = (Arrow) entityDamager;

			final List<MetadataValue> prevMeta = arrow.getMetadata("Murder");

			final Vector velocity = arrow.getVelocity();

			final Player shooter = (Player) arrow.getShooter();
			final MurderPlayer mpShooter = MurderPlayer.getPlayer(shooter);
			final Player damaged = (Player) entityDamaged;
			final MurderPlayer mpDamaged = MurderPlayer.getPlayer(damaged);

			if (mpDamaged.playing() && mpShooter.playing()) {
				if (shooter == damaged) {
					event.setCancelled(true);
				}
			}
			if (mpDamaged.inSpectate()) {
				this.arrowTeleport(damaged);
				try {
					damaged.setAllowFlight(true);
					damaged.setFlying(true);
				} catch (final Exception e) {
					e.printStackTrace();
				}

				final Arrow newArrow = shooter.launchProjectile(Arrow.class);
				newArrow.setShooter(shooter);
				newArrow.setVelocity(velocity);
				newArrow.setBounce(false);
				for (int i = 0; i < prevMeta.size(); i++) {
					newArrow.setMetadata("Murder", prevMeta.get(i));
				}

				event.setCancelled(true);
				arrow.remove();
				if (mpShooter.isMurderer()) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
						@Override
						public void run() {
							mpShooter.getArena().despawnKnifes();
							final Item item = newArrow.getLocation().getWorld().dropItemNaturally(newArrow.getLocation().add(0.0D, 1.0D, 0.0D), Items.Knife(Messages.ITEM_KNIFE.name()));
							mpShooter.getArena().addItem(item);
							if (newArrow.isOnGround()) {
								newArrow.remove();
							}
						}
					}, 10L);
				}
			}
		}
	}

	public void arrowTeleport(Player p) {
		double x = 0.0D;
		double y = 0.0D;
		double z = 0.0D;
		if (p.getLocation().add(0.0D, 2.0D, 0.0D).getBlock().getType().equals(Material.AIR)) {
			y = 2.0D;
		}
		if (p.getLocation().add(1.0D, 0.0D, 0.0D).getBlock().getType().equals(Material.AIR)) {
			x = 1.0D;
		}
		if (p.getLocation().add(0.0D, 0.0D, 1.0D).getBlock().getType().equals(Material.AIR)) {
			z = 1.0D;
		}
		if (p.getLocation().add(-1.0D, 0.0D, 0.0D).getBlock().getType().equals(Material.AIR)) {
			x = -1.0D;
		}
		if (p.getLocation().add(0.0D, 0.0D, -1.0D).getBlock().getType().equals(Material.AIR)) {
			z = -1.0D;
		}
		p.teleport(p.getLocation().add(x, y, z));
	}

	@EventHandler
	public void onVehicleEntityCollision(VehicleEntityCollisionEvent e) {
		if (e.getEntity() instanceof Player) {
			final Player p = (Player) e.getEntity();
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.inSpectate()) {
				e.setCancelled(true);
			}
		}
	}

	public Vector calculateVelocity(Player p, Entity e) {
		final Location ploc = p.getLocation();
		final Location eloc = e.getLocation();

		final double px = ploc.getX();
		final double py = ploc.getY();
		final double pz = ploc.getZ();
		final double ex = eloc.getX();
		final double ey = eloc.getY();
		final double ez = eloc.getZ();

		double x = 0.0D;
		double y = 0.0D;
		double z = 0.0D;
		if (px > ex) {
			x = 0.5D;
		} else if (px < ex) {
			x = -0.5D;
		}
		if (py > ey) {
			y = 0.25D;
		} else if (py < ey) {
			y = -0.25D;
		}
		if (pz > ez) {
			z = 0.5D;
		} else if (pz < ez) {
			z = -0.5D;
		}
		return new Vector(x, y, z);
	}

	@EventHandler
	public void onHunger(FoodLevelChangeEvent e) {
		if (e.getEntity() instanceof Player) {
			final Player p = (Player) e.getEntity();
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.playing()) {
				if (!mp.isBystander()) {
					e.setFoodLevel(20);
				} else {
					e.setFoodLevel(6);
				}
				e.setCancelled(true);
			}
		}
	}

	private final ArrayList<Material> mats = new ArrayList<Material>(Arrays.asList(Material.BED, Material.BED_BLOCK, Material.DIODE, Material.REDSTONE_COMPARATOR));

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (ArenaManager.getByWorld(e.getPlayer().getWorld()) == null) { return; }
		ArenaManager.getByWorld(e.getPlayer().getWorld()).worldLogger.onInteract(e);

		if (MurderPlayer.getPlayer(e.getPlayer()).playing()) {
			if (e.getClickedBlock() != null) {
				if (this.mats.contains(e.getClickedBlock().getType())) {
					e.setCancelled(true);
				}
			}
		}
		if (MurderPlayer.getPlayer(e.getPlayer()).inSpectate()) {
			e.setCancelled(true);
		}
	}

	int gunTrail;

	@EventHandler
	public void onGunUse(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Action a = e.getAction();
		final ItemStack is = p.getInventory().getItemInHand();
		if (mp.inSpectate()) {
			e.setCancelled(true);
			return;
		}
		if (mp.playing() && a == Action.RIGHT_CLICK_AIR && is.getType().equals(Items.Gun(Messages.ITEM_GUN.forPlayer(p)).getType())) {
			if (p.getInventory().contains(Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)).getType())) {
				p.getInventory().remove(Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)).getType());
				p.getInventory().removeItem(new ItemStack[] { Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)) });
				p.getInventory().setItem(8, null);
				p.updateInventory();

				final double vX = p.getLocation().getDirection().getX() * 10D;
				final double vY = p.getLocation().getDirection().getY() * 10D;
				final double vZ = p.getLocation().getDirection().getZ() * 10D;
				final Arrow arrow;
				mp.getArena().despawnAllArrows();
				if (Murder.serverVersion.contains("1.7.2")) {
					arrow = p.launchProjectile(Arrow.class);
					arrow.setVelocity(new Vector(vX, vY, vZ));
				} else {
					arrow = p.launchProjectile(Arrow.class, new Vector(vX, vY, vZ));
					arrow.setCritical(true);
				}
				arrow.setVelocity(new Vector(vX, vY, vZ));
				arrow.setShooter(p);

				arrow.setMetadata("Murder", new FixedMetadataValue(Murder.getInstance(), String.valueOf("ITEMS_BULLET")));

				this.gunTrail = Bukkit.getScheduler().scheduleSyncRepeatingTask(Murder.getInstance(), new Runnable() {

					@Override
					public void run() {
						if (arrow != null) {
							if (mp.getArena() != null && !mp.getArena().getPlayers().isEmpty()) {
								try {
									ParticleEffect.FIREWORKS_SPARK.sendToPlayers(mp.getArena().getPlayers(), arrow.getLocation(), 0, 0, 0, 0, 1);
								} catch (Exception e) {
									e.printStackTrace();
								}
								Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

									@Override
									public void run() {
										if (arrow.isOnGround() || arrow.isDead() || arrow.getLocation().distance(p.getLocation()) > 100) {
											Bukkit.getScheduler().cancelTask(PlayerListener.this.gunTrail);
											arrow.remove();
										}
									}

								}, 15);
							}
						}
					}
				}, 0, 1);

				final long reloadTime = 3;

				mp.getArena().reloadTimer = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
					@Override
					public void run() {
						if (mp.getArena() != null) {
							Bukkit.getScheduler().cancelTask(mp.getArena().reloadTimer);
						}
						if (mp.playing() && p.getInventory().contains(Items.Gun(Messages.ITEM_GUN.forPlayer(p)))) {
							p.getInventory().setItem(8, Items.Bullet(Messages.ITEM_BULLET.forPlayer(p)));
							p.getInventory().setItem(4, Items.Gun(Messages.ITEM_GUN.forPlayer(p)));
						}
					}
				}, reloadTime * 20L);
			} else {
				p.sendMessage(Messages.NOTIFICATION_RELOAD.forPlayer(p));
			}
		}
	}

	@EventHandler
	public void onSpeedBoost(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Action a = e.getAction();
		final ItemStack is = p.getInventory().getItemInHand();
		if (mp.playing() && (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) && is.getType().equals(Items.SpeedBoost(Messages.ITEM_SPEED.forPlayer(p)).getType())) {
			p.setFoodLevel(20);
			p.setSprinting(true);
			mp.setCanSprint(true);

			Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

				@Override
				public void run() {
					p.setSprinting(false);
					p.setFoodLevel(6);
					mp.setCanSprint(false);
				}
			}, (5 + Murder.rd.nextInt(5)) * 20);
			Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

				@Override
				public void run() {
					p.setSprinting(false);
					p.setFoodLevel(6);
					mp.setCanSprint(false);
				}
			}, 10 * 20 + 5);

			Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					p.getInventory().remove(Items.SpeedBoost(Messages.ITEM_SPEED.forPlayer(p)).getType());
					p.getInventory().remove(Items.SpeedBoost(Messages.ITEM_SPEED.forPlayer(p)));
					p.getInventory().setItem(8, null);
					p.getItemInHand().setType(Material.AIR);
					p.getInventory().getItemInHand().setType(Material.AIR);
					p.getInventory().getItemInHand().setAmount(0);
					p.updateInventory();

					mp.getArena().speedTimer = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
						@Override
						public void run() {
							if (mp.inGame()) {
								if (!mp.isMurderer() && !mp.isWeaponBystander()) {
									if (!p.getInventory().contains(Items.Gun(Messages.ITEM_GUN.forPlayer(p)))) {
										p.getInventory().setItem(8, Items.SpeedBoost(Messages.ITEM_SPEED.forPlayer(p)));
									}
								}
							}
						}
					}, 20 * 20);
				}
			}, 1L);
		}
	}

	@EventHandler
	public void onKnifeThrow(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Action a = e.getAction();
		final ItemStack is = p.getInventory().getItemInHand();
		if (mp.playing() && a == Action.RIGHT_CLICK_AIR && is.getType().equals(Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)).getType())) {
			mp.getArena().despawnAllArrows();
			mp.getArena().despawnKnifes();
			final int r = Murder.rd.nextInt(8) + 3;
			final double vX = p.getLocation().getDirection().getX() * r;
			final double vY = p.getLocation().getDirection().getY() * r;
			final double vZ = p.getLocation().getDirection().getZ() * r;
			final Arrow arrow;
			if (Murder.serverVersion.contains("1.7.2")) {
				arrow = p.launchProjectile(Arrow.class);
				arrow.setVelocity(new Vector(vX, vY, vZ));
			} else {
				arrow = p.launchProjectile(Arrow.class, new Vector(vX, vY, vZ));
				arrow.setCritical(true);
			}
			arrow.setVelocity(new Vector(vX, vY, vZ));
			arrow.setShooter(p);

			arrow.setMetadata("Murder", new FixedMetadataValue(Murder.getInstance(), String.valueOf("ITEMS_KNIFE")));

			mp.getArena().trailTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(Murder.getInstance(), new Runnable() {

				@Override
				public void run() {
					if (mp.getArena() != null && mp.getArena().getPlayers() != null && !mp.getArena().getPlayers().isEmpty()) {
						if (!arrow.isDead()) {
							try {
								ParticleEffect.FIREWORKS_SPARK.sendToPlayers(mp.getArena().getPlayers(), arrow.getLocation(), 0, 0.01f, 0, 0, 5);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					final Location currLoc = arrow.getLocation();
					if (arrow.isDead()) {
						if (mp.getArena() != null) {
							if (mp.getArena().droppedKnife != null) {
								if (!mp.getArena().getPlayers().isEmpty()) {
									try {
										ParticleEffect.FLAME.sendToPlayers(mp.getArena().getPlayers(), mp.getArena().droppedKnife.getLocation(), 0, 0, 0, 0.01f, 5);
									} catch (Exception e) {
										e.printStackTrace();
									}
								} else {
									Bukkit.getScheduler().cancelTask(mp.getArena().trailTask);
								}
							} else {
								Bukkit.getScheduler().cancelTask(mp.getArena().trailTask);
							}

						}
					}
					if (arrow.isOnGround() || arrow.isDead() || arrow.getVelocity().equals(new Vector(0, 0, 0))) {
						Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

							@Override
							public void run() {
								if (!p.getInventory().contains(Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)))) {
									if (!isKnifeSpawned(arrow)) {
										if (mp.getArena() != null) {
											mp.getArena().despawnKnifes();
											final double velY = arrow.getLocation().getBlockY() > p.getEyeLocation().getBlockY() && arrow.getLocation().add(0, 1, 0).getBlock().getType() != Material.AIR ? -0.5 : 0.5;
											final Item item = arrow.getLocation().getWorld().dropItemNaturally(currLoc.add(0.0D, velY, 0.0D), Items.Knife(Messages.ITEM_KNIFE.name()));
											item.setVelocity(new Vector(0, velY / 2, 0));
											mp.getArena().droppedKnife = item;
											mp.getArena().addItem(item);
										}
									}
								}
								arrow.remove();

							}
						}, 5);

					}
				}
			}, 0, 1);

			Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					p.getInventory().remove(Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)).getType());
					p.getInventory().remove(Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)));
					p.getInventory().setItem(4, null);
					p.getItemInHand().setType(Material.AIR);
					p.getInventory().getItemInHand().setType(Material.AIR);
					p.getInventory().getItemInHand().setAmount(0);
					p.updateInventory();
				}
			}, 1L);

			mp.getArena().knifeTimer = Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
				@Override
				public void run() {
					if (mp.inGame()) {
						for (final Item it : mp.getArena().getItems()) {
							if (it.getType().equals(Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)).getType())) {
								it.remove();
							}
						}
						if (mp.isMurderer()) {
							p.getInventory().setItem(4, Items.Knife(Messages.ITEM_KNIFE.forPlayer(p)));
							if (mp.getArena().droppedKnife != null) {
								mp.getArena().droppedKnife.remove();
							}
						}
						Bukkit.getScheduler().cancelTask(mp.getArena().trailTask);
					}
				}
			}, 1200L);
		}
	}

	@SuppressWarnings("unused")
	private Vector getKnifeVelocity(BlockFace face) {
		Vector v = null;
		switch (face) {
			case DOWN:
				v = new Vector(0, -1, 0);
				break;
			case UP:
				v = new Vector(0, 1, 0);
				break;
			case WEST:
				v = new Vector(-1, 0, 0);
				break;
			case SOUTH:
				v = new Vector(0, 0, 1);
				break;
			case EAST:
				v = new Vector(1, 0, 0);
				break;
			case NORTH:
				v = new Vector(0, 0, -1);
			default:
				break;
		}
		return v;
	}

	private static boolean isKnifeSpawned(Entity entity) {
		for (final Entity ent : entity.getNearbyEntities(30, 30, 30)) {
			if (ent.getType().equals(EntityType.DROPPED_ITEM)) {
				if (((Item) ent).getItemStack().equals(Items.Knife(Messages.ITEM_KNIFE.name()))) { return true; }
			}
		}
		return false;
	}

	public void onProjectileHit(ProjectileHitEvent e) {
		final Projectile projectile = e.getEntity();
		if (projectile.hasMetadata("Murder")) {
			if (projectile.getMetadata("Murder").get(0).value().equals("ITEMS_KNIFE") || projectile.getMetadata("Murder").get(0).value().equals("ITEMS_BULLET")) {

				final List<Entity> ents = getNearbyPlayers(projectile, 1, 2, 1);
				if (ents != null && !ents.isEmpty()) {
					final Entity ent = ents.get(0);
					if (ent instanceof Player) {
						if (projectile.getShooter() != ent) {
							final Player p = (Player) ent;
							if (!MurderPlayer.getPlayer(p).inSpectate()) {
							}
						}
					}
				}

			}
		}
	}

	private static List<Entity> getNearbyPlayers(Entity ent, double x, double y, double z) {
		final List<Entity> players = new ArrayList<Entity>();
		for (final Entity e : ent.getNearbyEntities(x, y, z)) {
			if (e instanceof Player) {
				players.add(e);
			}
		}
		return players;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void footSteps(PlayerMoveEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Location loc = p.getLocation();
		ParticleEffect particle = ParticleEffect.FOOTSTEP;
		if (mp.playing() && mp.getArena().inGame() && mp.getArena().getMurderer() != null && p.isOnGround() && !mp.inSpectate() && e.getFrom().distance(e.getTo()) > 0.1D) {
			final Player murderer = mp.getArena().getMurderer().player();
			if (murderer != null) {
				try {
					particle.sendToPlayer(murderer, loc.add(0.0D, 0.1D, 0.0D), 0, 0, 0, 0.0f, 1);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	@EventHandler
	public void onSprint(PlayerToggleSprintEvent e) {
		final Player p = e.getPlayer();
		MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.inGame() && e.isSprinting() && mp.isBystander() && mp.getArena().inGame() && !mp.canSprint()) {
			e.setCancelled(true);
			Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

				@Override
				public void run() {
					p.setFoodLevel(6);
					p.setSprinting(false);
				}
			}, 1);
		}
	}

	@EventHandler
	public void onRegen(EntityRegainHealthEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (e.getRegainReason() == RegainReason.SATIATED && mp != null && mp.playing() && !mp.canSprint()) {
				e.setAmount(0.0D);
				e.setCancelled(true);
				p.setFoodLevel(6);
			}
		}
	}

	@EventHandler
	public void smokeTrail(PlayerMoveEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.inGame()) {
			if (mp.isMurderer() && !mp.inSpectate()) {
				if (mp.getArena().smoke) {
					final Location loc = p.getLocation();
					ParticleEffect particle = ParticleEffect.LARGE_SMOKE;
					try {
						particle.sendToPlayers(mp.getArena().getPlayers(), loc.add(0.0D, 1.0D, 0.0D), 0, 0.5f, 0, 0, 1);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

}
