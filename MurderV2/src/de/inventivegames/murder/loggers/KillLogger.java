/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.loggers;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.event.PlayerKillEvent;
import de.inventivegames.murder.game.MurderPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class KillLogger implements Listener {

	@EventHandler
	public void log(PlayerKillEvent e) {
		final Player p = e.getDeath();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		final Player killer = e.getKiller();
		final MurderPlayer mpKiller = MurderPlayer.getPlayer(killer);
		if (e.points()) {
			if (mpKiller.isWeaponBystander()) {
				if (mp.isBystander()) {
					mpKiller.subtractPoints(Murder.POINTS_MINUS);
					System.out.println("Subtracting " + Murder.POINTS_MINUS + " Points from " + killer.getName() + ": KilledInnocent[POINTS_MINUS]");
					return;
				}
				if (mp.isMurderer()) {
					if (!e.playersAlive()) {
						mpKiller.addPoints(Murder.POINTS_BYSTANDER_WIN_WEAPON);
						System.out.println("Adding " + Murder.POINTS_BYSTANDER_WIN_WEAPON + " Points to " + killer.getName() + ": KilledMurderer[POINTS_BYSTANDER_WIN_WEAPON]");
						for (final Player p1 : e.getPlayers()) {
							final MurderPlayer mp1 = MurderPlayer.getPlayer(p1);
							if (!mp1.isMurderer() && !mp1.isWeaponBystander()) {
								mp1.addPoints(Murder.POINTS_BYSTANDER_WIN);
								System.out.println("Adding " + Murder.POINTS_BYSTANDER_WIN + " Points to " + p1.getName() + ": BystandersWon[POINTS_BYSTANDER_WIN]");
							}
						}
					}
					return;
				}
			}
			if (mpKiller.isMurderer()) {
				mpKiller.addPoints(Murder.POINTS_PLUS);
				System.out.println("Adding " + Murder.POINTS_PLUS + " Points to " + killer.getName() + ": MurdererKill[POINTS_PLUS]");
				if (!e.playersAlive()) {
					mpKiller.addPoints(Murder.POINTS_MURDERER_WIN);
					System.out.println("Adding " + Murder.POINTS_MURDERER_WIN + " Points to " + killer.getName() + ": MurdererWon[POINTS_MURDERER_WIN]");
				}
				return;
			}
		}
	}

}
