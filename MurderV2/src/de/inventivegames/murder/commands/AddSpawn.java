/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.commands;

import de.inventivegames.murder.arena.Arena;
import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.game.SpawnType;
import de.inventivegames.util.message.Messages;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class AddSpawn implements CommandInterface {

	@Override
	public boolean onCommand(Player p, String[] args) {
		if (args.length != 3) {
			p.sendMessage(String.format(Messages.WRONG_USAGE.forPlayer(p), "§4/murder help§c"));
			return true;
		}
		int id;
		try {
			id = Integer.parseInt(args[1]);
		} catch (final NumberFormatException e) {
			p.sendMessage(String.format(Messages.WRONG_USAGE.forPlayer(p), "§4/murder help§c"));
			return true;
		}
		if (ArenaManager.getByID(Integer.parseInt(args[1])) == null) {
			p.sendMessage(Messages.ARENA_NOT_EXISTING.forPlayer(p));

			return true;
		}
		final Arena arena = ArenaManager.getByID(id);
		arena.addSpawnpoint(SpawnType.valueOf(args[2].toUpperCase()), p.getLocation());
		p.sendMessage(String.format(Messages.SPAWN_ADDED.forPlayer(p), args[2].toUpperCase(), arena.getSpawnpoint(SpawnType.valueOf(args[2].toUpperCase())).size(), "#" + arena.getID()));
		return true;
	}

	@Override
	public String permission() {
		return Permissions.ADDSPAWN.perm();
	}

	@Override
	public List<String> getCompletions(String[] args) {
		final List<String> list = new ArrayList<String>();
		if (args.length == 2) {
			list.addAll(Arrays.asList(TabCompletionHelper.getAvailableArenaIDs()));
		}
		if (args.length == 3) {
			for (final SpawnType type : SpawnType.values()) {
				list.add(type.toString());
			}
		}
		return list;
	}

	@Override
	public String getUsage() {
		return "§aaddSpawn §b<ArenaID> <SpawnType>";
	}

}
