/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.commands;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.util.message.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class CommandHandler implements CommandExecutor, TabCompleter, Listener {

	private final HashMap<String, CommandInterface> commands;

	public CommandHandler() {
		this.commands = new HashMap<String, CommandInterface>();
		this.loadCommands();
	}

	private void loadCommands() {
		this.commands.put("join", new Join());
		this.commands.put("leave", new Leave());
		this.commands.put("addarena", new AddArena());
		this.commands.put("help", new Help());
		this.commands.put("removearena", new RemoveArena());
		this.commands.put("addspawn", new AddSpawn());
		this.commands.put("start", new ForceStart());
		this.commands.put("stop", new Stop());
		this.commands.put("reload", new Reload());
		this.commands.put("setname", new SetName());
		this.commands.put("savearena", new SaveArena());
		this.commands.put("arenainfo", new ArenaInfo());
		this.commands.put("playerinfo", new PlayerInfo());
		this.commands.put("force", new Force());
	}

	public ArrayList<String> getCommandList() {
		return new ArrayList<String>(this.commands.keySet());
	}

	public HashMap<String, CommandInterface> getCommandMap() {
		return new HashMap<String, CommandInterface>(this.commands);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(Murder.getPrefix() + "Only Players can use this Command!");
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("murder")) {
			final Player p = (Player) sender;

			if (args.length != 0 && args[0] != null) {
				final String sub = args[0].toLowerCase();
				if (sub.equalsIgnoreCase("test")) {
					ArenaManager.getByID(1).worldLogger.resetModifiedBlocks();
					return true;
				}
				if (!this.commands.containsKey(sub)) {
					p.sendMessage(Messages.UNKNOWN_COMMAND.forPlayer(p));
					return true;
				}
				if (!p.hasPermission(this.commands.get(sub).permission()) && !p.isOp()) {
					p.sendMessage(Messages.NO_PERMISSION.forPlayer(p));
					return true;
				}
				try {
					this.commands.get(sub).onCommand(p, args);
				} catch (final Exception e) {
					e.printStackTrace();

				}
			} else {
				p.sendMessage("§2=== Murder MiniGame Version " + Murder.getInstance().getDescription().getVersion().toString() + " by inventivetalent ===");
				if (p.hasPermission(Permissions.PLAYER.perm())) {
					p.sendMessage("§cType §4/murder help §cfor a list of commands!");
				}
			}
			return true;
		}
		return false;
	}

	ArrayList<String> empty = new ArrayList<String>();

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String paramString, String[] args) {
		if (!(sender instanceof Player)) { return this.empty; }
		if (!cmd.getName().equalsIgnoreCase("murder")) { return this.empty; }
		final Player p = (Player) sender;

		final List<String> completions = new ArrayList<String>();

		if (args.length == 1) {
			for (final Entry<String, CommandInterface> e : this.commands.entrySet()) {
				if (p.hasPermission(e.getValue().permission())) {
					completions.add(e.getKey());
				}
			}
		}
		if (args.length >= 2) {
			if (this.commands.get(args[0]) == null || !p.hasPermission(this.commands.get(args[0].toLowerCase()).permission())) { return this.empty; }
			completions.addAll(this.commands.get(args[0].toLowerCase()).getCompletions(args));
		}

		return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, completions.toArray(new String[completions.size()]));
	}

	@EventHandler
	public void preprocess(PlayerCommandPreprocessEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(p);
		if (mp.playing()) {
			if (!e.getMessage().toLowerCase().contains("murder")) {
				if (!Murder.getInstance().getConfig().getList("allowedCommands").contains(e.getMessage().toLowerCase()) && !p.hasPermission("murder.admin")) {
					p.sendMessage(Messages.CANT_USE_COMMAND.forPlayer(p));
					e.setCancelled(true);
				}
			}
		}
	}

}
