package de.inventivegames.murder.commands;

import de.inventivegames.murder.arena.ArenaManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Class to help with the TabCompletion for Bukkit.
 *
 * @author D4rKDeagle
 */
public class TabCompletionHelper {
	public static List<String> getPossibleCompletionsForGivenArgs(String[] args, String[] possibilitiesOfCompletion) {
		final String argumentToFindCompletionFor = args[args.length - 1];

		final List<String> listOfPossibleCompletions = new ArrayList<String>();
		for (int i = 0; i < possibilitiesOfCompletion.length; i++) {
			final String[] foundString = possibilitiesOfCompletion;
			try {
				if (foundString[i] != null && foundString[i].regionMatches(true, 0, argumentToFindCompletionFor, 0, argumentToFindCompletionFor.length())) {
					listOfPossibleCompletions.add(foundString[i]);
				}
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		Collections.sort(listOfPossibleCompletions);

		return listOfPossibleCompletions;
	}

	public static String[] getOnlinePlayerNames() {
		final String[] onlinePlayerNames = new String[Bukkit.getServer().getOnlinePlayers().size()];
		Iterator<? extends Player> iterator = Bukkit.getOnlinePlayers().iterator();
		int i = 0;
		while (iterator.hasNext()) {
			Player p = iterator.next();
			onlinePlayerNames[i++] = p.getName();
		}

		return onlinePlayerNames;
	}

	public static String[] getMaterialNames() {
		final Material[] mats = Material.values();
		final String[] materialNames = new String[Material.values().length];
		for (int i = 0; i < Material.values().length; i++) {
			if (mats[i].isBlock()) {
				materialNames[i] = mats[i].toString();
			}
		}
		return materialNames;
	}

	public static String[] getAllIntegers() {
		final String[] ints = new String[25];
		for (int i = 1; i < 30; i++) {
			ints[i] = "" + i;
		}
		return ints;
	}

	public static String[] getAvailableArenaIDs() {
		final String[] ints = new String[ArenaManager.getArenas().size()];
		for (int i = 1; i <= ArenaManager.getArenas().size(); i++) {
			if (ArenaManager.getArenas().get(i - 1) != null) {
				ints[i - 1] = "" + i;
			}
		}
		return ints;
	}
}
