/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.arena;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.Signs;
import de.inventivegames.murder.event.GameLeaveEvent;
import de.inventivegames.murder.event.PlayerKillEvent;
import de.inventivegames.murder.game.*;
import de.inventivegames.murder.loggers.WorldChangeLogger;
import de.inventivegames.util.message.Messages;
import org.bukkit.*;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class Arena {

	private final int         id;
	private       String      name;
	private       ArenaStatus status;
	private final List<Player> players = new ArrayList<Player>();
	private final File              file;
	public final  YamlConfiguration config;
	private       World             world;
	private final HashMap<SpawnType, ArrayList<Spawnpoint>> spawnpoints = new HashMap<SpawnType, ArrayList<Spawnpoint>>();
	private       boolean                                   starting    = false;
	private       boolean                                   inGame      = false;
	public  Game         game;
	private MurderPlayer murderer;
	private final List<MurderPlayer> weaponBystanders = new ArrayList<MurderPlayer>();
	private final List<Item>         loot             = new ArrayList<Item>();
	public        boolean            smoke            = false;
	private final List<Item>         items            = new ArrayList<Item>();
	public        boolean            started          = false;

	// Loggers
	public WorldChangeLogger worldLogger;

	// Schedulers
	public int  knifeTimer;
	public int  reloadTimer;
	public int  speedTimer;
	public int  trailTask;
	public Item droppedKnife;
	private boolean removed = false;

	public Arena(int id) {
		this.id = id;
		this.file = new File("plugins/Murder/Arenas/" + id + "/arena.yml");
		this.config = YamlConfiguration.loadConfiguration(this.file);
		this.status = ArenaStatus.WAITING;
		this.game = new Game(this);

		this.worldLogger = new WorldChangeLogger(this);
	}

	public void join(final Player p) {
		final Location loc = this.getSpawnpoint(SpawnType.LOBBY).get(0).toLocation();
		loc.getWorld().loadChunk(loc.getBlockX(), loc.getBlockZ());
		p.teleport(loc);

		p.setVelocity(new Vector(0, 0.25, 0));
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

			@Override
			public void run() {
				p.setVelocity(new Vector(0, 0.1, 0));

			}
		}, 10L);
		this.players.add(p);
		this.sendMessage(Messages.JOIN_ARENA, p.getName(), this.getPlayerAmount(), Murder.maxPlayers);
		if (this.getPlayerAmount() >= Murder.minPlayers) {
			if (!this.started) {
				this.game.startDelayed();
				this.started = true;
			}
		}

		Signs.updateSigns(this.getID());
		if (this.getPlayerAmount() == Murder.maxPlayers) {
			this.setStatus(ArenaStatus.FULL);
		}

	}

	public void leave(final Player p, boolean broadcast, boolean v) {
		this.players.remove(p);
		final MurderPlayer mp = MurderPlayer.getPlayer(p);

		if (v) {
			Murder.getInstance().getServer().getPluginManager().callEvent(new GameLeaveEvent(this, p));
		}

		Signs.updateSigns(this.getID());

		if (v) {
			if (broadcast) {
				this.sendMessage(Messages.LEAVE_ARENA, p.getName());
			}
			if (this.getAlivePlayerAmount() == 1) {
				if (!this.game.stopping) {
					if (!MurderPlayer.getPlayer(this.getAlivePlayers().get(0)).inGame() || !this.inGame()) {
						this.game.cancelDelayedStart();
						this.started = false;
						return;
					}
					this.game.stopDelayed(20 * 2);
					if (MurderPlayer.getPlayer(this.getAlivePlayers().get(0)).isMurderer()) {
						this.sendMessage(Messages.WIN_MURDERER_A, new Object[0]);
						if (this.getMurderer().getNameTag() != null) {
							this.sendMessage(Messages.WIN_MURDERER_INFO, this.getMurderer().getNameTag(), this.getMurderer().getNameTag().substring(0, 2) + this.getMurderer().player().getName());
						}

						Murder.getInstance().getServer().getPluginManager().callEvent(new PlayerKillEvent(this, true, "murderer", this.getMurderer().player(), p, false));
					}
					for (final Player p1 : this.getMurdererBlacklist()) {
						Murder.murdererBlacklist.remove(p1);
					}
					for (final Player p1 : this.getWeaponBlacklist()) {
						Murder.weaponBlacklist.remove(p1);
					}
				}

			} else {
				if (this.getMurderer() == mp) {
					if (this.inGame()) {
						if (!this.game.stopping) {
							if (this.getBystanderAmount() >= 2) {
								this.sendMessage(Messages.WIN_BYSTANDER_A, new Object[0]);
								this.sendMessage(Messages.WIN_MURDERER_INFO, mp.getNameTag(), p.getName());
								this.game.stopDelayed(20 * 2);
								for (final Player p1 : this.getMurdererBlacklist()) {
									Murder.murdererBlacklist.remove(p1);
								}
								for (final Player p1 : this.getWeaponBlacklist()) {
									Murder.weaponBlacklist.remove(p1);
								}
							}
						}
					}
				}
				if (this.getWeaponBystanders().contains(mp)) {
					if (this.inGame()) {
						if (!this.game.stopping) {
							this.removeWeaponBystander(mp);

							Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
								@Override
								public void run() {
									final Item gun = Arena.this.world.dropItemNaturally(mp.player().getLocation(), Items.Gun(Messages.ITEM_GUN.name()));
									Arena.this.addItem(gun);
								}
							}, 2L);
						}
					}
				}
			}
			if (this.getPlayerAmount() <= 0) {
				this.game.stopDelayed(1);
			}

		}
	}

	public void addEffects() {
		for (final Player pl : this.getPlayers()) {
			pl.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 2147000, 255));
			pl.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 2147000, 255));
			for (Player p : this.getPlayers()) {
				if (p != pl) {
					p.hidePlayer(pl);
				}
			}

			pl.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2147000, 128));
			pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2147000, 255));
			pl.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 2147000, 255));

			pl.getInventory().setHelmet(new ItemStack(Material.PUMPKIN));

			pl.playSound(pl.getEyeLocation(), Sound.GHAST_SCREAM, 5, 1);

		}
	}

	public void addItem(Item i) {
		this.items.add(i);
	}

	public void removeItem(Item i) {
		this.items.remove(i);
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setWorld(World w) {
		this.world = w;
	}

	public void sendMessage(String message) {
		for (final Player p : this.getPlayers()) {
			p.sendMessage(message);
		}
	}

	public void sendMessage(de.inventivegames.util.message.Messages msg, Object... format) {
		try {
			for (final Player p : this.getPlayers()) {
				String message = msg.forPlayer(p);
				p.sendMessage(String.format(message, format));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendRawMessage(String raw) {
		try {
			for (final Player p : this.getPlayers()) {
				MurderPlayer.getPlayer(p).sendRawMessage(raw);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendSpectatorMessage(String message) {
		for (final Player p : this.getPlayers()) {
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.inSpectate()) {
				p.sendMessage(message);
			}
		}
	}

	public void create() {
		this.config.options().copyDefaults(true);
		this.config.options().header("Do NOT Edit this File manually, unless you know how to properly edit JSON Strings!");
		this.config.addDefault("Data", null);
		this.config.addDefault("Reset", null);
		try {
			this.config.save(this.file);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public void setMurderer(MurderPlayer mp) {
		this.murderer = mp;
	}

	public void addWeaponBystander(MurderPlayer mp) {
		if (!this.weaponBystanders.contains(mp)) {
			this.weaponBystanders.add(mp);
		}
	}

	public void removeWeaponBystander(MurderPlayer mp) {
		this.weaponBystanders.remove(mp);
	}

	public void starting(boolean b) {
		this.starting = b;
	}

	public boolean starting() {
		return this.starting;
	}

	public void inGame(boolean b) {
		this.inGame = b;
	}

	public boolean inGame() {
		return this.inGame;
	}

	public void addLoot(Item item) {
		this.loot.add(item);
	}

	public void removeLoot(Item item) {
		this.loot.add(item);
	}

	public List<Item> getLoot() {
		return this.loot;
	}

	public void addSpawnpoint(SpawnType type, Location loc) {
		final List<Spawnpoint> list = new ArrayList<Spawnpoint>();
		if (this.spawnpoints != null && !this.spawnpoints.isEmpty() && this.spawnpoints.get(type) != null) {
			list.addAll(this.spawnpoints.get(type));
			if (type == SpawnType.LOBBY) {
				list.clear();
			}
		}
		list.add(new Spawnpoint(loc));
		this.spawnpoints.remove(type);
		this.spawnpoints.put(type, (ArrayList<Spawnpoint>) list);
	}

	public List<Spawnpoint> getSpawnpoint(SpawnType type) {
		return this.spawnpoints.get(type);
	}

	public boolean spawnpointExists(SpawnType type) {
		if (type.equals(SpawnType.LOBBY)) {
			if (this.spawnpoints.get(SpawnType.LOBBY) != null && this.spawnpoints.get(SpawnType.LOBBY).get(0) != null) { return true; }
			return false;
		}
		if (type.equals(SpawnType.PLAYERS)) {
			if (this.spawnpoints.get(SpawnType.PLAYERS).get(0) != null) { return true; }
			return false;
		}
		if (type.equals(SpawnType.LOOT)) {
			if (this.spawnpoints.get(SpawnType.LOOT).get(0) != null) { return true; }
			return false;
		}
		return false;
	}

	public void setSpawnpoints(Map<SpawnType, ArrayList<Spawnpoint>> map) {
		this.spawnpoints.clear();
		this.spawnpoints.putAll(map);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public int getPlayerAmount() {
		return this.players.size();
	}

	public int getBystanderAmount() {
		int i = 0;
		for (final Player p : this.getAlivePlayers()) {
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (mp.isBystander()) {

				i++;

			}
		}
		return i;
	}

	public int getID() {
		return this.id;
	}

	public void setStatus(ArenaStatus status) {
		this.status = status;
	}

	public ArenaStatus getStatus() {
		return this.status;
	}

	public void updatePlayerList() {
		ArrayList<Player> temp = new ArrayList<Player>();
		for (Player p : this.players) {
			if (p.isOnline()) {
				temp.add(p);
			}
		}
		this.players.clear();
		this.players.addAll(temp);

	}

	public List<Player> getPlayers() {
		this.updatePlayerList();
		return new ArrayList<Player>(this.players);
	}

	public World getWorld() {
		return this.world;
	}

	public void addPlayer(Player p) {
		this.players.add(p);
	}

	public void removePlayer(Player p) {
		this.players.remove(p);
	}

	public List<Player> getAlivePlayers() {
		final List<Player> players = new ArrayList<Player>();
		for (final Player p : this.getPlayers()) {
			final MurderPlayer mp = MurderPlayer.getPlayer(p);
			if (!mp.inSpectate()) {
				players.add(p);
			}
		}
		return players;
	}

	public int getAlivePlayerAmount() {
		return this.getAlivePlayers().size();
	}

	public void save() {
		if (!this.removed) {
			this.config.set("Data", this.toString());
			try {
				this.config.save(this.file);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void despawnAllItems() {
		for (final Entity ent : this.world.getEntities()) {
			if (ent.getType() == EntityType.DROPPED_ITEM) {
				ent.remove();
			}
		}
	}

	public void despawnAllArrows() {
		for (final Entity ent : this.world.getEntities()) {
			if (ent.getType() == EntityType.ARROW) {
				ent.remove();
			}
		}
	}

	public void despawnKnifes() {
		for (final Entity ent : this.world.getEntities()) {
			if (ent.getType() == EntityType.DROPPED_ITEM) {
				if (((Item) ent).getItemStack().getType() == Items.Knife(Messages.ITEM_KNIFE.name()).getType()) {
					ent.remove();
				}
			}
		}
	}

	public MurderPlayer getMurderer() {
		return this.murderer;
	}

	public List<MurderPlayer> getWeaponBystanders() {
		return this.weaponBystanders;
	}

	public List<String> getWeaponBystanderNames() {
		final List<String> names = new ArrayList<String>();
		for (final MurderPlayer mp : this.getWeaponBystanders()) {
			names.add(mp.player().getName());
		}
		return names;
	}

	public List<Player> getForcedMurderers() {
		final List<Player> list = new ArrayList<Player>();
		for (final Player p : Murder.forcedMurderers) {
			if (MurderPlayer.getPlayer(p).getArena() == this) {
				list.add(p);
			}
		}
		return list;
	}

	public List<Player> getForcedWeaponBystanders() {
		final List<Player> list = new ArrayList<Player>();
		for (final Player p : Murder.forcedWeapons) {
			if (MurderPlayer.getPlayer(p).getArena() == this) {
				list.add(p);
			}
		}
		return list;
	}

	public List<Player> getMurdererBlacklist() {
		final List<Player> list = new ArrayList<Player>();
		for (final Player p : Murder.murdererBlacklist) {
			if (MurderPlayer.getPlayer(p).getArena() == this) {
				list.add(p);
			}
		}
		return list;
	}

	public List<Player> getWeaponBlacklist() {
		final List<Player> list = new ArrayList<Player>();
		for (final Player p : Murder.weaponBlacklist) {
			if (MurderPlayer.getPlayer(p).getArena() == this) {
				list.add(p);
			}
		}
		return list;
	}

	public void sendInfo(Player sender) {
		sender.sendMessage("§7---------------------------------");
		sender.sendMessage("§7>> §2Arena §a#" + this.getID() + "§2/§a\"" + this.getName() + "\"§2 §7<<");
		sender.sendMessage("§7---------------------------------");
		sender.sendMessage("§7World=§a" + this.getWorld().getName());
		sender.sendMessage("§7Status=§a" + this.getStatus());
		MurderPlayer.getPlayer(sender).sendRawMessage(this.generatePlayersString("Players", this.getPlayers()));
		MurderPlayer.getPlayer(sender).sendRawMessage(this.generatePlayersString("AlivePlayers", this.getAlivePlayers()));
		sender.sendMessage("§7PlayerAmount=§a" + this.getPlayerAmount());
		sender.sendMessage("§7AlivePlayerAmount=§a" + this.getAlivePlayerAmount());
		sender.sendMessage("§7BystanderAmount=§a" + this.getBystanderAmount());
		sender.sendMessage("§7Murderer=§a" + (this.getMurderer() != null ? this.getMurderer().player().getName() : ""));
		sender.sendMessage("§7WeaponBystander=§a" + (this.getWeaponBystanders() != null ? this.getWeaponBystanderNames() : ""));
		sender.sendMessage("§7---------------------------------");

	}

	private String generatePlayersString(String s, List<Player> players) {
		final StringBuilder sb = new StringBuilder();
		sb.append("{\"text\":\"\",\"extra\":[{\"text\":\"" + s + "=\",\"color\":\"gray\"}");
		for (final Player p : players) {
			if (!players.get(0).equals(p)) {
				sb.append(",{\"text\":\", \",\"color\":\"gray\"}");
			}
			sb.append(",{\"text\":\"" + p.getName() + "\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/murder playerinfo " + p.getName() + "\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"§7Click here to show Information for §a" + p.getName() + "§7.\"}}");
		}
		sb.append("]}");
		return sb.toString();
	}

	public void setRemoved(boolean b) {
		this.removed = b;
	}

	// ////////////

	@Override
	@SuppressWarnings("unchecked")
	public String toString() {
		final JSONObject object = new JSONObject();
		object.put("id", this.id);
		object.put("name", this.name);
		object.put("world", this.world.getName());
		object.put("spawnpoints", MapToJSON(this.spawnpoints));
		return object.toJSONString();
	}

	@SuppressWarnings("rawtypes")
	private static String MapToJSON(Map map) {
		final StringWriter out = new StringWriter();
		try {
			JSONValue.writeJSONString(map, out);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		final String jsonText = out.toString();
		return jsonText;
	}

	public static Arena fromString(String s) {
		Arena arena;
		JSONObject property = new JSONObject();
		try {
			property = (JSONObject) new JSONParser().parse(s);
		} catch (final ParseException e) {
			e.printStackTrace();
		}

		final long id = (long) property.get("id");
		arena = new Arena((int) id);
		final String name = (String) property.get("name");
		arena.setName(name);
		final World world = Murder.getInstance().getServer().getWorld((String) property.get("world"));
		arena.setWorld(world);

		world.setDifficulty(Difficulty.NORMAL);
		world.setGameRuleValue("doMobSpawning", "false");
		world.setGameRuleValue("naturalRegeneration", "false");

		final Map<SpawnType, ArrayList<Spawnpoint>> points = StringToMap(((String) property.get("spawnpoints")).replaceAll("\\\"", "\""));
		arena.setSpawnpoints(points);

		return arena;
	}

	private static HashMap<SpawnType, ArrayList<Spawnpoint>> StringToMap(String s) {
		try {
			final JSONObject obj = (JSONObject) new JSONParser().parse(s);
			return StringToMap(obj);
		} catch (final ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static HashMap<SpawnType, ArrayList<Spawnpoint>> StringToMap(JSONObject s) {
		final HashMap<SpawnType, ArrayList<Spawnpoint>> map = new HashMap<SpawnType, ArrayList<Spawnpoint>>();

		JSONObject property = s;

		for (final Object o : property.entrySet()) {
			final String[] sA = o.toString().split("=");
			final List<Spawnpoint> points = new ArrayList<Spawnpoint>();
			JSONArray obj = new JSONArray();
			try {
				obj = (JSONArray) new JSONParser().parse(sA[1]);
			} catch (final ParseException e) {
				e.printStackTrace();
			}
			for (final Object s1 : obj.toArray()) {
				points.add(Spawnpoint.fromString(String.valueOf(s1).replace("\\\"", "\"")));
			}
			map.put(SpawnType.valueOf(sA[0]), (ArrayList<Spawnpoint>) points);
		}

		return map;
	}

	public void reset() {
		this.status = ArenaStatus.WAITING;
		this.players.clear();
		this.game.stopping = false;
		this.game.stopped = false;

		this.inGame = false;
		this.murderer = null;
		this.weaponBystanders.clear();
		this.loot.clear();
		this.smoke = false;
		this.items.clear();
		this.game.gameWon = false;

		for (final Player p1 : this.getMurdererBlacklist()) {
			Murder.murdererBlacklist.remove(p1);
		}
		for (final Player p1 : this.getWeaponBlacklist()) {
			Murder.weaponBlacklist.remove(p1);
		}

		Bukkit.getScheduler().cancelTask(this.game.smokeDelay);
		Bukkit.getScheduler().cancelTask(this.knifeTimer);
		Bukkit.getScheduler().cancelTask(this.speedTimer);
		Bukkit.getScheduler().cancelTask(this.reloadTimer);
		Bukkit.getScheduler().cancelTask(this.trailTask);

		for (final Item i : this.loot) {
			i.remove();
		}
		this.loot.clear();

		for (final Item i : this.items) {
			i.remove();
		}

		this.items.clear();

		this.smoke = false;

		this.game.cancelAllTaks();

		this.setMurderer(null);
		this.despawnAllArrows();
		this.despawnAllItems();
		this.despawnKnifes();

		this.worldLogger.resetModifiedBlocks();

		this.status = ArenaStatus.WAITING;

	}

	public boolean exists() {
		return this.file.exists();
	}

}
