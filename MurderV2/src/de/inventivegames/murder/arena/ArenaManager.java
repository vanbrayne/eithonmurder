/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.arena;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.event.ArenaCreateEvent;
import de.inventivegames.murder.event.ArenaRemoveEvent;
import de.inventivegames.util.message.Messages;
import net.minecraft.util.org.apache.commons.io.FileUtils;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class ArenaManager {

	private static List<Arena> arenas = new ArrayList<Arena>();

	public static void loadArenas() {
		Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§fLoading Arenas...");
		if (!new File("plugins/Murder/Arenas/").exists()) { return; }
		if (new File("plugins/Murder/Arenas/").list().length == 0) {
			Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§cNo Arenas found!");
		}
		for (final String s : new File("plugins/Murder/Arenas/").list()) {
			final File f = new File("plugins/Murder/Arenas/" + s);
			if (f.isDirectory()) {
				final File file = new File("plugins/Murder/Arenas/" + s + "/arena.yml");
				if (file.exists()) {
					final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
					final Arena arena = Arena.fromString(config.getString("Data"));
					if (!arenas.contains(arena)) {
						Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§fLoading Arena #" + s + "...");
						arenas.add(arena);
					}
				}
			}
		}
	}

	public static void unloadArenas() {
		arenas.clear();
	}

	public static void unloadArena(Arena arena) {
		if (arenas.contains(arena)) {
			arenas.remove(arena);
		}
	}

	public static List<Arena> getArenas() {
		return new ArrayList<Arena>(arenas);
	}

	public static void saveArenas() {
		Murder.getConsole().sendMessage(Murder.getDebugPrefix() + "§fSaving Arenas...");
		for (final Arena arena : arenas) {
			arena.save();
		}
	}

	public static Arena getByID(int id) {
		for (final Arena arena : arenas) {
			if (arena.getID() == id) { return arena; }
		}
		return null;
	}

	public static Arena getByName(String name) {
		for (final Arena arena : arenas) {
			if (arena.getName().equals(name)) { return arena; }
		}
		return null;
	}

	public static Arena getByWorld(World world) {
		if (world == null) { return null; }
		for (final Arena arena : arenas) {
			if (arena.getWorld() != null) {
				if (arena.getWorld().equals(world)) { return arena; }
			}
		}
		return null;
	}

	public static int getIDByName(String name) {
		return getByName(name).getID();
	}

	public static String getNameByID(int id) {
		return getByID(id).getName();
	}

	public static Arena createArena(int id, Player p) {
		final World world = p.getWorld();
		final Arena arena = new Arena(id);

		world.setDifficulty(Difficulty.NORMAL);
		world.setGameRuleValue("doMobSpawning", "false");
		world.setGameRuleValue("naturalRegeneration", "false");
		if (!arena.exists()) {
			arena.setWorld(world);
			arena.create();

			p.sendMessage(String.format(Messages.ARENA_ADDED.forPlayer(p), id));

			Murder.getInstance().getServer().getPluginManager().callEvent(new ArenaCreateEvent(arena, p));
			arenas.add(arena);
			return arena;
		} else {
			p.sendMessage(String.format(Messages.ARENA_EXISTING.forPlayer(p), id));
			return null;
		}
	}

	public static void removeArena(Arena arena, Player p) {
		if (arena.exists()) {
			final File file = new File("plugins/Murder/Arenas/" + arena.getID() + "/arena.yml");
			if (file.exists()) {
				final File file1 = new File("plugins/Murder/Arenas/" + arena.getID() + "/signs.yml");
				arena.setRemoved(true);
				file.delete();
				file1.delete();
				try {
					FileUtils.deleteDirectory(new File("plugins/Murder/Arenas/" + arena.getID()));
				} catch (final IOException e) {
					e.printStackTrace();
				}

				p.sendMessage(String.format(Messages.ARENA_REMOVED.forPlayer(p), "#" + arena.getID()));
				Murder.getInstance().getServer().getPluginManager().callEvent(new ArenaRemoveEvent(arena, p));
			}
		}
	}

}
