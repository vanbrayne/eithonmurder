/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.murder.bungeecord;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.inventivegames.murder.Murder;
import de.inventivegames.murder.arena.ArenaManager;
import de.inventivegames.murder.commands.Permissions;
import de.inventivegames.murder.event.GameCancelEvent;
import de.inventivegames.murder.event.GameEndEvent;
import de.inventivegames.murder.event.GameLeaveEvent;
import de.inventivegames.murder.game.MurderPlayer;
import de.inventivegames.murder.misc.ResourcePack;
import de.inventivegames.util.message.Messages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;

/**
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public class BungeeCordHandler implements Listener {

	private ArrayList<String> RPreset = new ArrayList<>();

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		final MurderPlayer mp = MurderPlayer.getPlayer(e.getPlayer());
		if (this.RPreset.contains(p.getUniqueId().toString().replace("-", ""))) {

			ResourcePack.resetResourcePack(p);
			Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {

				@Override
				public void run() {
					sendPlayerToHub(p.getName());

					BungeeCordHandler.this.RPreset.remove(p.getUniqueId().toString().replace("-", ""));
				}
			}, 10);

			return;
		}

		if (mp.playing()) {
			p.sendMessage(Messages.PLAYER_INGAME.forPlayer(p));
			return;
		}
		final int id = Murder.bungeeArena;
		if (ArenaManager.getByID(id) == null) {
			p.sendMessage(Messages.ARENA_NOT_EXISTING.forPlayer(p));
			return;
		}
		if (ArenaManager.getByID(id).inGame()) {
			p.sendMessage(Messages.ARENA_INGAME.forPlayer(p));
			return;
		}
		if (!p.hasPermission(Permissions.JOIN.perm() + "." + id)) {
			p.sendMessage(String.format(Messages.NO_PERMISSION_JOIN.forPlayer(p), ArenaManager.getByID(id).getID()));
			return;
		}
		mp.joinArena(ArenaManager.getByID(id));
	}

	@EventHandler
	public void onGameEnd(GameEndEvent e) {
		for (final Player p : e.getPlayers()) {
			ResourcePack.resetResourcePack(p);
			sendPlayerToHub(p.getName());
		}
	}

	@EventHandler
	public void onGameCancel(GameCancelEvent e) {
		for (final Player p : e.getPlayers()) {
			ResourcePack.resetResourcePack(p);
			sendPlayerToHub(p.getName());
		}
	}

	@EventHandler
	public void onGameLeave(final GameLeaveEvent e) {
		ResourcePack.resetResourcePack(e.getPlayer());
		sendPlayerToHub(e.getPlayer().getName());

		this.playerQuit(e.getPlayer());
	}

	private void playerQuit(final Player p) {
		this.RPreset.add(p.getUniqueId().toString().replace("-", ""));
		Bukkit.getScheduler().scheduleSyncDelayedTask(Murder.getInstance(), new Runnable() {
			@Override
			public void run() {
				getPlayerFromHub(p.getName());
			}
		}, 10);
	}

	private static void sendPlayerToHub(String name) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("ConnectOther");
		out.writeUTF(name);
		out.writeUTF(Murder.bungeeHub);
		if (Bukkit.getOnlinePlayers().size() > 0) {
			Bukkit.getOnlinePlayers().iterator().next().sendPluginMessage(Murder.getInstance(), "BungeeCord", out.toByteArray());
		}
	}

	private static void getPlayerFromHub(String name) {
		final ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("ConnectOther");
		out.writeUTF(name);
		out.writeUTF(Murder.bungeeGame);
		if (Bukkit.getOnlinePlayers().size() > 0) {
			Bukkit.getOnlinePlayers().iterator().next().sendPluginMessage(Murder.getInstance(), "BungeeCord", out.toByteArray());
		}
	}

}
