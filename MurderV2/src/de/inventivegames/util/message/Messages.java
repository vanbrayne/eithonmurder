/*
 * Copyright 2015 Marvin Schäfer (inventivetalent). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */

package de.inventivegames.util.message;

import de.inventivegames.murder.Murder;
import de.inventivegames.murder.event.LanguageRequestEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Simple enum-Class to manage configurable Messages for multiple languages.
 * <p>
 * <p>
 * Please provide credits, if you decide to use this for your own projects.
 * <p>
 * © Copyright 2013-2015 inventivetalent
 *
 * @author inventivetalent
 */
public enum Messages {

	ARENA_INGAME("&cArena is Ingame!"),
	ARENA_FULL("&cArena is Full!"),
	ARENA_NOT_INGAME("&cArena isn't ingame!"),
	PLAYER_INGAME("&cYou are already Ingame!"),
	PLAYER_NOT_INGAME("&cYou are not Ingame!"),
	NOT_ENOUGH_PLAYERS("&cNot enough Players to start!"),
	LOBBY_SPAWN_NOT_EXISTING("&cLobby Spawnpoint does not exist!"),
	JOIN_ARENA("%1$s &2joined the Arena! &6%2$s&7/&6%3$s"),
	LEAVE_ARENA("%1$s &2left the Arena!"),
	COUNTDOWN_LOBBY("&2Lobby ends in %1$s Seconds"),
	COUNTDOWN_GAME("&2Game starts in %1$s Seconds"),
	BYSTANDER("&l&9You are a Bystander."),
	BYSTANDER_WEAPON("&l&9You are a Bystander with a secret Weapon."),
	BYSTANDER_WEAPON_NOW("&l&9You are now a Bystander with a secret Weapon."),
	MURDERER("&c&lYou are the Murderer."),
	TITLE_IDENTITY(Prefix.NONE, "{\"text\":\"\",\"extra\":[{\"text\":\"Your secret Identity is §f%s\",\"color\":\"gray\"}]}"),
	TITLE_BYSTANDER(Prefix.NONE, "{\"text\":\"\",\"extra\":[{\"text\":\"You are a Bystander\",\"color\":\"blue\"}]}"),
	TITLE_BYSTANDER_WEAPON(Prefix.NONE, "{\"text\":\"\",\"extra\":[{\"text\":\"with a secret Weapon\",\"color\":\"blue\"}]}"),
	TITLE_MURDERER(Prefix.NONE, "{\"text\":\"\",\"extra\":[{\"text\":\"You are the Murderer\",\"color\":\"red\"}]}"),
	OBJECTIVE_BYSTANDER(Prefix.NONE, "&e&lSurvive &e&land try to find out who the &c&lMurderer &e&lis!"),
	OBJECTIVE_BYSTANDER_WEAPON(Prefix.NONE, "&e&lFind the &c&lMurderer &e&land kill them with your &9&lsecret Weapon&e!"),
	OBJECTIVE_MURDERER(Prefix.NONE, "&e&lKill all &9&lBystanders&e&l, but don't act suspicious!"),
	SPECTATOR("&aYou are now a Spectator."),
	ITEM_KNIFE(Prefix.NONE, "&cKnife"),
	ITEM_GUN(Prefix.NONE, "&9Gun"),
	ITEM_BULLET(Prefix.NONE, "&7Bullet"),
	ITEM_LOOT(Prefix.NONE, "&7Loot"),
	ITEM_SPEED(Prefix.NONE, "&7Speed-Boost"),
	ITEM_TELEPORTER(Prefix.NONE, "&7Teleporter"),
	NO_PERMISSION("&cYou don't have Permission to execute this command!"),
	NO_PERMISSION_JOIN("&cYou don't have Permission to join this Arena (#%s)!"),
	WRONG_USAGE("&cWrong usage! Type %1$s for a list of commands!"),
	UNKNOWN_COMMAND("&cUnknown Command!"),
	CANT_USE_COMMAND("&cYou can't use this Command while you're Ingame!"),
	SIGN_CREATED("&2Sucessfully created Murder Sign!"),
	ARENA_ADDED("&2Successfully added Arena %1$s"),
	ARENA_REMOVED("&2Successfully removed Arena %1$s"),
	ARENA_EXISTING("&cArena %1$s already exists!"),
	ARENA_NOT_EXISTING("&cArena does not exist!"),
	SPAWN_ADDED("&2Successfully added %1$s Spawnpoint %2$s for Arena %3$s"),
	SPAWN_UNKNOEN("&cUnknown SpawnPoint type!"),
	SPAWN_AVAILABLE("&2Available types"),
	LOOT_COLLECTED("&2You collected %1$s Loot."),
	DISGUISED("&2You disguised as %1$s"),
	WIN_MURDERER_A("&cMurderer &2wins!"),
	WIN_BYSTANDER_A("&9Bystanders &2win!"),
	WIN_MURDERER_INFO("&2The &cMurderer &2was&r %1$s &a/&1 %2$s"),
	KILL_MURDERER("%1$s &2killed the &cMurderer."),
	KILL_BYSTANDER("&1%1$s &2killed an innocent Bystander."),
	KILL_INNOCENT("&1%1$s &2killed an innocent Bystander."),
	NOTIFICATION_RELOAD("&cYou have to wait for the Gun to reload!"),
	NOTIFICATION_DISGUISE("&2Right Click to disguise as this Player for 1 Loot"),
	NOTIFICATON_SMOKE("&aOther Players can now recognize you as the Murderer."),
	NOTIFICATION_LOOT("&cYou don't have enough Loot to disguise as this Player!"),
	RESOURCEPACK_DOWNLOAD_TRY("&6Trying to download Resourcepack."),
	RESOURCEPACK_DOWNLOAD("&6Downloading Resourcepack..."),
	RESOURCEPACK_SUCCESS("&6Successfully downloaded the Resourcepack!"),
	RESOURCEPACK_FAILED("&cThe download of the Resourcepack failed."),
	RESOURCEPACK_DECLINED("&cYou have to accept the Resourcepack to play Murder!");

	private final Prefix               prefix;
	private final String               key;
	private final String               defaultMessage;
	private final AlternativeMessage[] alternatives;

	private Messages(String defaultMessage) {
		this(Prefix.DEFAULT, defaultMessage);
	}

	private Messages(Prefix pre, String defaultMessage) {
		this(pre, defaultMessage, new AlternativeMessage[0]);
	}

	private Messages(String defaultMessage, AlternativeMessage... alternatives) {
		this(Prefix.DEFAULT, defaultMessage, alternatives);
	}

	private Messages(Prefix pre, String defaultMessage, AlternativeMessage... alternatives) {
		this.prefix = pre;
		this.key = this.name().toLowerCase();
		this.defaultMessage = defaultMessage;
		this.alternatives = alternatives;
	}

	public String forPlayer(CommandSender sender) {
		String lang;
		if (sender instanceof Player) {
			lang = getLocale((Player) sender);
		} else {
			lang = DEFAULT_LANGUAGE;
		}
		return this.forLanguage(lang);
	}

	public String forLanguage(String lang) {
		if (lang == null) {
			lang = DEFAULT_LANGUAGE;
		}
		if (lang.length() > 2) { throw new IllegalArgumentException("Maximum allowed Language-code length is 2!"); }
		String path = lang + "." + this.key;
		if (!config.contains(path)) {
			this.addUnknown(lang);
			lang = DEFAULT_LANGUAGE;
			path = lang + "." + this.key;
		}
		return this.prefix.toString() + formatFromConfig(config.getString(path));
	}

	public static ArrayList<String> getLanguages() {
		return (ArrayList<String>) config.getStringList("languages");
	}

	public static ArrayList<String> getUnknownLanguages() {
		return (ArrayList<String>) config.getStringList("unknown");
	}

	public static String getDefaultLanguage() {
		return DEFAULT_LANGUAGE;
	}

	public static String getSpecialMessage(String key, Player player) {
		return getSpecialMessage(key, getLocale(player));
	}

	public static String getSpecialMessage(String key, String lang) {
		if (lang == null) {
			lang = DEFAULT_LANGUAGE;
		}
		return Prefix.DEFAULT + formatFromConfig(config.getString(lang + "." + key, "§c<Message not found>"));
	}

	static File              config_file;
	static YamlConfiguration config;
	public static String DEFAULT_LANGUAGE = "en";

	public static void init(File file) throws FileNotFoundException, IOException, InvalidConfigurationException {
		config_file = file;
		config = new YamlConfiguration();
		if (!file.exists()) {
			file.createNewFile();
		}
		config.load(file);

		config.addDefault("languages", new ArrayList<String>(Arrays.asList(DEFAULT_LANGUAGE)));
		ArrayList<String> languages = (ArrayList<String>) config.getStringList("languages");

		config.addDefault("unknown", new ArrayList<String>());

		for (Messages m : values()) {
			if (m.alternatives.length > 0) {
				for (AlternativeMessage alt : m.alternatives) {
					config.addDefault(alt.lang + "." + m.key, formatToConfig(alt.message));
				}
			}
			for (String lang : languages) {
				config.addDefault(lang + "." + m.key, formatToConfig(m.defaultMessage));
			}
		}

		config.options().copyDefaults(true);
		config.save(config_file);
	}

	private void addUnknown(String lang) {
		ArrayList<String> langs = (ArrayList<String>) config.getStringList("unknown");
		if (!langs.contains(lang)) {
			langs.add(lang);
			config.set("unknown", langs);
			try {
				config.save(config_file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	static String formatToConfig(String s) {
		if (s == null) { return s; }
		s = s.replaceAll("(§([a-fk-or0-9]))", "&$2");

		s = s.replace("§", ":ae:");
		s = s.replace("§", ":ue:");
		s = s.replace("§", ":oe:");

		s = s.replace("§", ":AE:");
		s = s.replace("§", ":UE:");
		s = s.replace("§", ":OE:");

		s = s.replace("§", ":ss:");
		return s;
	}

	static String formatFromConfig(String s) {
		if (s == null) { return s; }
		s = s.replaceAll("(&([a-fk-or0-9]))", "§$2");

		s = s.replace(":ae:", "§");
		s = s.replace(":ue:", "§");
		s = s.replace(":oe:", "§");

		s = s.replace(":AE:", "§");
		s = s.replace(":UE:", "§");
		s = s.replace(":OE:", "§");

		s = s.replace(":ss:", "§");
		return s;
	}

	/**
	 * Get the Clientside Language if a Player
	 *
	 * @param p Player
	 * @return Client Language
	 */
	public static String getLocale(@Nonnull Player p) {
		Object locale = null;
		try {
			Object handle = Reflection.getHandle(p);
			locale = Reflection.getField(handle.getClass(), "locale").get(handle);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		if (locale != null) {
			locale = ((String) locale).substring(0, 2);
		}
		LanguageRequestEvent event = new LanguageRequestEvent(p, (String) locale);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.getLanguage().equals(locale)) {
			locale = event.getLanguage().substring(0, 2);
		}
		return (String) (locale != null ? locale : "en");
	}

	public static enum Prefix {
		NONE(""),
		DEFAULT(Murder.getPrefix());

		private String s;

		private Prefix(String s) {
			this.s = s;
		}

		@Override
		public String toString() {
			return this.s;
		}
	}

	static class AlternativeMessage {

		String lang, message;

		AlternativeMessage(String lang, String message) {
			if (lang.length() > 2) { throw new IllegalArgumentException("Maximum allowed Language-code length is 2!"); }
			this.lang = lang;
			this.message = message;
		}

	}

	/**
	 * @author inventivetalent
	 */
	public static class Reflection {

		public static String getVersion() {
			String name = Bukkit.getServer().getClass().getPackage().getName();
			String version = name.substring(name.lastIndexOf('.') + 1) + ".";
			return version;
		}

		public static Class<?> getNMSClass(String className) {
			String fullName = "net.minecraft.server." + getVersion() + className;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(fullName);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return clazz;
		}

		public static Class<?> getOBCClass(String className) {
			String fullName = "org.bukkit.craftbukkit." + getVersion() + className;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(fullName);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return clazz;
		}

		public static Class<?> getOBCClass(String className, String subPackage) {
			String fullName = "org.bukkit.craftbukkit." + getVersion() + subPackage + "." + className;
			Class<?> clazz = null;
			try {
				clazz = Class.forName(fullName);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return clazz;
		}

		public static Object getHandle(Object obj) {
			try {
				return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		public static Field getField(Class<?> clazz, String name) {
			try {
				Field field = clazz.getDeclaredField(name);
				field.setAccessible(true);
				return field;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
			for (Method m : clazz.getMethods()) {
				if (m.getName().equals(name) && (args.length == 0 || ClassListEqual(args, m.getParameterTypes()))) {
					m.setAccessible(true);
					return m;
				}
			}
			return null;
		}

		public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2) {
			boolean equal = true;
			if (l1.length != l2.length) { return false; }
			for (int i = 0; i < l1.length; i++) {
				if (l1[i] != l2[i]) {
					equal = false;
					break;
				}
			}
			return equal;
		}
	}

}
